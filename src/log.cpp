#include "log.h"

namespace spn {

std::ofstream &logger() {
  static std::ofstream file("log.log", std::ios::binary);
  return file;
}

}  // namespace spn