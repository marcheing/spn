#ifndef _SPN_CONFIG_H_
#define _SPN_CONFIG_H_

#include <boost/property_tree/ptree.hpp>
#include <string>

namespace spn {

class Config {
 public:
  Config(const Config &config) = delete;
  Config(Config &&config) = delete;
  Config &operator=(const Config &) = delete;
  Config &operator=(Config &&) = delete;
  ~Config() = default;

  const static Config &instance() {
    static Config config;
    return config;
  }

  template <typename T>
  T get(const std::string &key) const {
    return _pt.get<T>(key);
  }

  const boost::property_tree::ptree &node(const std::string &key) const;

 private:
  Config();

  boost::property_tree::ptree _pt;
};

}  // namespace spn

#endif
