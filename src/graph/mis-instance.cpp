#include "graph/mis-instance.h"
#include "log.h"
#include "node/multinomial.h"
#include "node/product.h"
#include "node/sum.h"

#include <algorithm>
#include <fstream>
#include <numeric>
#include <sstream>

using std::string;
using std::stringstream;

namespace spn::graph {

MISInstance::MISInstance(const char *filename) : Base() {
  std::ifstream file(filename, std::ios::in);
  if (!file) {
    logger() << "Error opening file " << filename << "." << std::endl;
    std::exit(EXIT_FAILURE);
  }

  string line;

  std::getline(file, line);

  // Ignore comments
  for (; line.front() == 'c'; std::getline(file, line)) {
  };

  string p, edges;

  stringstream first_line_stream(line);
  first_line_stream >> p >> edges >> _n_vertices >> _n_edges;
  for (uint64_t i = 0; i < _n_vertices; i++) {
    std::vector<bool> row(_n_vertices, false);
    _data.emplace_back(row);
  }

  for (uint64_t i = 0; i < _n_edges; i++) {
    std::getline(file, line);
    stringstream edge_stream(line);

    string e;
    uint64_t v1, v2;

    edge_stream >> e >> v1 >> v2;

    // They start from 1 and not from 0
    v1--;
    v2--;
    _data[v1][v2] = true;
    _data[v2][v1] = true;
  }

  file.close();
}

void MISInstance::to_file(const char *filename) const {
  std::ofstream file(filename, std::ios::out);
  if (!file) {
    logger() << "Error opening file " << filename << "." << std::endl;
    std::exit(EXIT_FAILURE);
  }

  file << "p edge " << _n_vertices << ' ' << _n_edges << '\n';

  for (uint64_t i = 0; i < _n_vertices; i++) {
    for (uint64_t j = i; j < _n_vertices; j++) {
      if (_data[i][j]) {
        file << "e " << i + 1 << ' ' << j + 1 << '\n';
      }
    }
  }
  file.close();
}

const node::SPN *MISInstance::to_spn() const {
  std::unordered_map<uint64_t, std::vector<uint64_t>> neighbors;
  for (size_t i = 0; i < _data.size(); i++) {
    neighbors[i] = std::vector<uint64_t>();
    for (size_t j = 0; j < _data[i].size(); j++) {
      if (_data[i][j]) {
        neighbors[i].emplace_back(j);
      }
    }
  }
  std::vector<const node::SPN *> product_nodes(_data.size());
  std::vector<size_t> number_of_neighbors(_data.size());
  auto root_node = new node::Sum();

  std::vector<size_t> counts_for_c(_data.size());

  for (size_t i = 0; i < _data.size(); i++) {
    number_of_neighbors[i] =
        static_cast<size_t>(std::count(_data[i].begin(), _data[i].end(), true));
    counts_for_c[i] = static_cast<size_t>(exp2l(_data.size() - number_of_neighbors[i] - 1));
    auto product_node = new node::Product();
    for (size_t j = 0; j < _data.size(); j++) {
      const double p = (i == j ? 1 : (_data[i][j] ? 0 : 0.5));
      product_node->add_child(new node::Multinomial(j, std::vector<double>{1. - p, p}));
      logger() << "Add leaf for var " << j << " with parent " << i << " with p = " << p
               << std::endl;
    }
    product_nodes[i] = product_node;
  }
  const size_t c =
      static_cast<size_t>(std::accumulate(counts_for_c.begin(), counts_for_c.end(), 0));
  for (size_t i = 0; i < _data.size(); i++) {
    long double weight = exp2l(_data.size() - number_of_neighbors[i] - 1) / c;
    root_node->add_child_with_weight(product_nodes[i], static_cast<double>(weight));
  }
  return root_node;
}
}  // namespace spn::graph
