#ifndef _SPN_GRAPH_BASE_H_
#define _SPN_GRAPH_BASE_H_

#include <cstdint>
#include <vector>

namespace spn::graph {

class Base {
 public:
  Base() : _n_vertices(), _n_edges(), _data(){};
  uint64_t n_vertices() const { return _n_vertices; }
  uint64_t n_edges() const { return _n_edges; }
  virtual ~Base() = default;

 protected:
  uint64_t _n_vertices;
  uint64_t _n_edges;
  std::vector<std::vector<bool>> _data;
};

}  // namespace spn::graph

#endif
