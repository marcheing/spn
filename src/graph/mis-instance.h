#ifndef _SPN_GRAPH_MIS_INSTANCE_H
#define _SPN_GRAPH_MIS_INSTANCE_H

#include "graph/base.h"
#include "node/spn.h"

namespace spn::graph {

class MISInstance : public Base {
 public:
  explicit MISInstance(const char *filename);
  void to_file(const char *filename) const;
  const node::SPN *to_spn() const;
};

}  // namespace spn::graph

#endif
