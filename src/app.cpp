#include "app.h"
#include "actions/base.h"
#include "config.h"
#include "log.h"

#include <boost/property_tree/ptree.hpp>
#include <chrono>
#include <functional>

namespace spn {

using boost::property_tree::ptree;

App::App() : _version("0.0.0-alpha") {}

void App::start() const {
  init_log();
  execute_actions();
}

void App::init_log() const { logger() << "SPN application\nVersion: " << _version << std::endl; }

void App::execute_actions() const {
  std::function<void(const ptree &)> action;
  for (auto nodes_tree : Config::instance().node("actions")) {
    for (auto params : nodes_tree.second) {
      const std::string &action_name = params.first;
      action = actions::action_named(action_name);
      std::chrono::high_resolution_clock::time_point before =
          std::chrono::high_resolution_clock::now();
      action(params.second);
      std::chrono::high_resolution_clock::time_point after =
          std::chrono::high_resolution_clock::now();
      logger()
          << "--------------------------------------------------------------------------------\n";
      logger() << "Action " << action_name << " took "
               << std::chrono::duration_cast<std::chrono::microseconds>(after - before).count()
               << " microseconds.\n";
      logger()
          << "--------------------------------------------------------------------------------\n";
    }
  }
}
}  // namespace spn
