#ifndef _SPN_PARSER_H_
#define _SPN_PARSER_H_

#include "data.h"
#include "variable.h"

#include <string>
#include <unordered_map>
#include <vector>

namespace spn {

struct ParsedData {
  std::unordered_map<uint64_t, Variable> scope;
  std::vector<std::unordered_map<uint64_t, uint64_t>> cvntmap;
};

Data parse_data(const char *filename);

}  // namespace spn

#endif
