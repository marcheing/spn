#ifndef _SPN_LEARN_GENS_H_
#define _SPN_LEARN_GENS_H_

#include "node/spn.h"
#include "variable.h"

#include <unordered_map>
#include <vector>

namespace spn::learn {

node::SPN *gens(const std::unordered_map<uint64_t, Variable> &scope,
                const std::vector<std::unordered_map<uint64_t, uint64_t>> &data, int kclusters, double pval,
                double eps, size_t mp);

}  // namespace spn::learn

#endif
