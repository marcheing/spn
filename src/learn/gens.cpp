#include "learn/gens.h"
#include "cluster.h"
#include "indep/graph.h"
#include "indep/union-find.h"
#include "log.h"
#include "node/multinomial.h"
#include "node/product.h"
#include "node/sum.h"
#include "vardata.h"
#include "variable.h"

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

using std::endl;
using std::unordered_map;
using std::vector;

namespace spn::learn {

node::Multinomial *create_leaf(const Variable &variable,
                               const vector<unordered_map<uint64_t, uint64_t>> &data) {
  logger() << "Creating new leaf..." << endl;
  vector<uint64_t> counts(variable.n_categories, 0);
  for (auto data_sample : data) {
    counts[data_sample.at(variable.var_id)]++;
  }
  return new node::Multinomial(variable.var_id, counts);
}

node::Product *fully_factorized_form(const vector<unordered_map<uint64_t, uint64_t>> &data,
                                     const unordered_map<uint64_t, Variable> &scope) {
  // Fully factorized form. All instances are approximately the same
  auto product_node = new node::Product();
  for (const auto &[_, variable] : scope) {
    product_node->add_child(create_leaf(variable, data));
  }
  return product_node;
}

node::SPN *gens(const unordered_map<uint64_t, Variable> &scope,
                const vector<unordered_map<uint64_t, uint64_t>> &data, const int kclusters,
                const double pval, const double eps, const size_t mp) {
  uint64_t n = scope.size();

  logger() << "Sample size: " << data.size() << ", scope size: " << n << endl;

  // If the data's scope is unary, then we return a leaf (univariate
  // distribution)
  if (n == 1ul) {
    return create_leaf((*scope.begin()).second, data);
  }

  // Else we check for independent subsets of variables. We separate variables
  // in k partitions, where every partition is pairwise independent with each
  // other.

  logger() << "Creating VarDatas for Independency Test..." << endl;
  // Do not return vdata or use it outside this function
  vector<VarData> vdata(n);
  uint64_t l = 0;
  uint64_t tn = data.size();

  for (const auto &[var_id, var] : scope) {
    // tdata is the transpose of data[k]
    vector<uint64_t> tdata(tn);
    for (uint64_t j = 0; j < tn; j++) {
      tdata[j] = data.at(j).at(var_id);
    }
    // FIXME: Must guarantee that tdata is copied in here
    vdata[l] = VarData{var_id, var.n_categories, tdata};
    l++;
  }

  logger() << "Creating new Independency graph..." << endl;
  indep::Graph independency_graph = indep::union_find_independency_graph(vdata, pval);
  vdata.clear();

  // If true, then we can partition the set of variables in data into
  // independent subsets. This means we can create a product node (since
  // product nodes' children have disjoint scopes).
  if (independency_graph.kset.size() > 1) {
    logger() << "Found independency. Separating independent sets." << endl;
    auto product_node = new node::Product();
    vector<vector<uint64_t>> &kset = independency_graph.kset;
    tn = data.size();

    for (const auto &subset : kset) {
      // Data slices of the relevant sectors
      vector<unordered_map<uint64_t, uint64_t>> tdata(tn);

      // Create new scope with new variables
      unordered_map<uint64_t, Variable> the_new_scope;

      // Number of variables in set of variables kset[i]
      for (uint64_t j = 0; j < tn; j++) {
        tdata[j] = unordered_map<uint64_t, uint64_t>();
        for(const auto &set_member : subset) {
          // Get the instantiations of variables in kset[i]
          tdata[j][set_member] = data.at(j).at(set_member);
          the_new_scope[set_member] = Variable{set_member, scope.at(set_member).n_categories};
        }
      }

      // Adds the recursive calls as children of this new product node
      product_node->add_child(gens(the_new_scope, tdata, kclusters, pval, eps, mp));
    }
    return product_node;
  }

  // Else we perform k-clustering on the instances
  logger() << "No independence found. Preparing for clustering..." << endl;

  uint64_t m = data.size();
  vector<vector<uint64_t>> mdata(m);
  for (uint64_t i = 0; i < m; i++) {
    uint64_t lc = data[i].size();
    mdata[i] = vector<uint64_t>(lc);
    size_t ll = 0;
    vector<uint64_t> keys(lc);
    for (std::pair<uint64_t, int> k : data[i]) {
      keys[ll] = k.first;
      ll++;
    }
    std::sort(keys.begin(), keys.end());
    for (uint64_t j = 0; j < lc; j++) {
      mdata[i][j] = data.at(i).at(keys[j]);
    }
  }

  vector<unordered_map<uint64_t, const vector<uint64_t> *>> clusters;

  if (kclusters > 0) {
    logger() << "data: " << data.size() << ", mdata: " << mdata.size() << endl;
    if (static_cast<int>(mdata.size()) < kclusters) {
      return fully_factorized_form(data, scope);
    }
    clusters = kmedoid(static_cast<size_t>(kclusters), mdata);
  } else if (kclusters == -1) {
    clusters = dbscan(mdata, eps, mp);
  } else {
    clusters = optics(mdata, eps, mp);
  }

  uint64_t k = clusters.size();

  if (k == 1ul) {
    return fully_factorized_form(data, scope);
  }
  mdata = vector<vector<uint64_t>>();

  logger() << "Reformating clusters to appropriate format and creating sum node..." << endl;

  auto sum_node = new node::Sum();
  for (uint64_t i = 0; i < k; i++) {
    uint64_t ni = clusters[i].size();
    logger() << "cluster " << i << " has " << ni << " elements" << endl;
    vector<unordered_map<uint64_t, uint64_t>> ndata(ni);

    size_t ll = 0;
    for (std::pair<uint64_t, const vector<uint64_t> *> kk : clusters[i]) {
      ndata[ll] = unordered_map<uint64_t, uint64_t>();
      for (std::pair<uint64_t, uint64_t> value : data[kk.first]) {
        ndata[ll][value.first] = value.second;
      }
      ll++;
    }

    logger() << "Created new sum node child. Recursing..." << endl;
    sum_node->add_child_with_weight(gens(scope, ndata, kclusters, pval, eps, mp),
                                    static_cast<double>(ni) / static_cast<double>(data.size()));
  }

  clusters = vector<unordered_map<uint64_t, const vector<uint64_t> *>>();
  return sum_node;
}
}  // namespace spn::learn
