#ifndef _SPN_LOG_H_
#define _SPN_LOG_H_

#include <fstream>
#include <string>
#include <unordered_map>

namespace spn {

std::ofstream &logger();

template <typename T, typename V>
void log_unordered_map(std::unordered_map<T, V> map) {
  for (const auto &[key, value] : map) {
    logger() << key << ": " << value << ' ';
  }
  logger() << std::endl;
}

}  // namespace spn

#endif
