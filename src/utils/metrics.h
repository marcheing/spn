#ifndef _SPN_UTILS_METRICS_H_
#define _SPN_UTILS_METRICS_H_

#include <cstdint>
#include <vector>

namespace spn::utils {

// Computes the Euclidean distance between two sets of instances
uint64_t hamming(const std::vector<uint64_t> &p1, const std::vector<uint64_t> &p2);

// Computes the Euclidean distance between two ordered sets of instances
double euclidean(const std::vector<uint64_t> &p1, const std::vector<uint64_t> &p2);

}  // namespace spn::utils

#endif
