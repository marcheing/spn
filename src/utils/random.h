#ifndef _SPN_UTILS_RANDOM_H_
#define _SPN_UTILS_RANDOM_H_

#include "config.h"

#include <random>
#include <vector>

namespace spn::utils {

template <typename T>
T generate_random(T lowest, T highest) {
  static std::random_device random_device;
  static std::mt19937 random_generator(Config::instance().get<uint64_t>("random.seed"));
  std::uniform_int_distribution<T> distribution(lowest, highest);
  return distribution(random_generator);
}

template <typename T>
std::vector<T> generate_random_vector_without_repetition(T lowest, T highest, size_t size) {
  std::vector<T> vector;
  for (size_t i = 0; i < size; i++) {
    T number = generate_random<T>(lowest, highest);
    if (std::find(vector.begin(), vector.end(), number) == vector.end()) {
      vector.emplace_back(number);
    } else {
      i--;
    }
  }
  return vector;
}

}  // namespace spn::utils

#endif
