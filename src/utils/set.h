#ifndef _SPN_UTILS_SET_H_
#define _SPN_UTILS_SET_H_

#include <algorithm>
#include <unordered_set>
#include <vector>

namespace spn::utils {
template <typename T>
std::vector<T> to_vector(const std::unordered_set<T> &set) {
  std::vector<T> vector(set.size());
  std::transform(set.begin(), set.end(), vector.begin(), [](const T &element) { return element; });
  return vector;
}

}  // namespace spn::utils

#endif
