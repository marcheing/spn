#ifndef _SPN_UTILS_LOG_H_
#define _SPN_UTILS_LOG_H_

#include <unordered_map>
#include "log.h"

namespace spn::utils {
template <typename K, typename V>
void log_map(const std::unordered_map<K, V> &map) {
  logger() << "{ ";
  for (const auto &[key, value] : map) {
    logger() << key << ": " << value << ' ';
  }
  logger() << '}' << std::endl;
}

template <typename T>
void log_vector(const std::vector<T> &vector) {
  logger() << "[ ";
  for (const auto &element : vector) {
    logger() << element << ' ';
  }
  logger() << ']' << std::endl;
}
}  // namespace spn::utils

#endif
