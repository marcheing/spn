#include "utils/file.h"

#include <fstream>
#include <stdexcept>

using std::string;

namespace spn::utils {

string file_to_string(const char *filename) {
  std::ifstream file(filename, std::ios::in | std::ios::ate);
  if (!file) {
    throw std::runtime_error("File could not be opened.");
  }
  auto size = static_cast<size_t>(file.tellg());
  string text(size, '\0');
  file.seekg(0);
  file.read(&text[0], static_cast<std::streamsize>(size));
  return text;
}

}  // namespace spn::utils
