#ifndef _SPN_UTILS_VECTOR_H_
#define _SPN_UTILS_VECTOR_H_

#include <algorithm>
#include <vector>

namespace spn::utils {

template <typename T, class Compare>
std::vector<T> inplace_best_k(std::vector<T> &original, size_t k, Compare cmp) {
  std::sort(original.begin(), original.end(), cmp);
  if (k < original.size()) {
    original.erase(original.begin() + static_cast<int64_t>(k), original.end());
  }
  return original;
}

template <typename T, class Compare>
std::vector<T> best_k(std::vector<T> original, size_t k, Compare cmp) {
  return inplace_best_k(original, k, cmp);
}

template <typename T>
std::vector<T> first_k(const std::vector<T> &v, size_t k) {
  if (k == 0) return {};
  size_t vector_size = v.size();
  if (k >= vector_size) return v;
  std::vector<T> returning(k);
  for (size_t i = 0; i < k; i++) {
    returning[i] = v[i];
  }
  return returning;
}

template <typename T>
std::vector<std::vector<T>> segment(const std::vector<T> &v, size_t k) {
  if (k == 0) return {};
  size_t vector_size = v.size();
  if (k >= vector_size) return {v};
  size_t number_of_subvectors = 1 + ((vector_size - 1) / k);
  std::vector<std::vector<T>> subvectors = std::vector<std::vector<T>>(number_of_subvectors);
  auto iterator = v.begin();
  for (size_t i = 0; i < number_of_subvectors; i++) {
    std::vector<T> subvector(k);
    for (size_t j = 0; j < k && iterator != v.end(); j++) {
      subvector[j] = *iterator;
      iterator++;
    }
    subvector.shrink_to_fit();
    subvectors[i] = subvector;
  }
  return subvectors;
}

template <typename T>
std::vector<std::vector<T>> combine_vector(typename std::vector<T>::iterator first,
                                 typename std::vector<T>::iterator last, size_t size) {
  if (size == 0) return {};
  if (size == 1) {
    std::vector<std::vector<T>> returning(static_cast<size_t>(std::distance(first, last)));
    std::transform(first, last, returning.begin(), [](const T &x) { return std::vector<T>{x}; });
    return returning;
  }
  std::vector<std::vector<T>> returning;
  for (auto list_iter = first; static_cast<size_t>(std::distance(list_iter, last)) >= (size - 1);
       list_iter++) {
    for (const std::vector<T> &other_combination : combine_vector<T>(list_iter + 1, last, size - 1)) {
      std::vector<T> current{*list_iter};
      for (const T &number : other_combination) {
        current.emplace_back(number);
      }
      returning.emplace_back(current);
    }
  }
  return returning;
}

}  // namespace spn::utils

#endif
