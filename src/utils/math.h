#ifndef _SPN_UTILS_MATH_H_
#define _SPN_UTILS_MATH_H_

#include "utils/random.h"

#include <cstdint>
#include <limits>
#include <vector>

namespace spn::utils {

const double LOGZERO = -std::numeric_limits<double>::infinity();

template <typename T>
std::vector<T> random_sample(uint64_t count, std::vector<T> &original) {
  std::vector<T> sample;
  size_t original_max_index = original.size() - 1;
  for (size_t i = 0; i < count; i++) {
    sample.emplace_back(original.at(generate_random<size_t>(0, original_max_index)));
  }
  return sample;
}

template <typename T>
T random_element(std::vector<T> &vector) {
  return vector.at(generate_random<size_t>(0, vector.size() - 1));
}

}  // namespace spn::utils

#endif
