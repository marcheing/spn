#ifndef _SPN_UTILS_FILE_H_
#define _SPN_UTILS_FILE_H_

#include <string>

namespace spn::utils {

std::string file_to_string(const char *filename);
}

#endif
