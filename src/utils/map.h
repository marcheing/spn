#ifndef _SPN_UTILS_MAP_H_
#define _SPN_UTILS_MAP_H_

#include <algorithm>
#include <unordered_map>
#include <vector>

namespace spn::utils {

template <typename T, typename S>
bool has_key(const std::unordered_map<T, S> &map, const T &key) {
  try {
    map.at(key);
    return true;
  } catch (std::out_of_range &) {
    return false;
  }
}

template <typename T, typename S>
std::vector<T> keys(const std::unordered_map<T, S> &map) {
  std::vector<T> vector;
  for (auto &[key, _] : map) {
    vector.emplace_back(key);
  }
  return vector;
}

template <typename T, typename S>
std::vector<S> values(const std::unordered_map<T, S> &map) {
  std::vector<S> vector;
  for (auto &[_, value] : map) {
    vector.emplace_back(value);
  }
  return vector;
}

template <typename K, typename V, typename VAR>
std::vector<std::unordered_map<K, V>> combine_map(typename std::vector<VAR>::iterator first,
                                        typename std::vector<VAR>::iterator last,
                                        const std::unordered_map<VAR, std::vector<V>> &var_values) {
  size_t distance = static_cast<size_t>(std::distance(first, last));
  if (distance == 0) {
    return {};
  }
  VAR var = *first;
  const std::vector<V> &values = var_values.at(var);
  if (distance == 1) {
    std::vector<std::unordered_map<K, V>> returning;
    std::transform(values.begin(), values.end(), std::back_inserter(returning),
                   [var](const V &value) {
                     return std::unordered_map<K, V>{{var, value}};
                   });
    return returning;
  }
  std::vector<std::unordered_map<K, V>> returning;
  std::vector<std::unordered_map<K, V>> other_variables = combine_map<K, V, VAR>(first + 1, last, var_values);
  for (const V &value : values) {
    for (auto &other_vars_map : other_variables) {
      std::unordered_map<K, V> sub_map{{var, value}};
      sub_map.merge(std::unordered_map<K, V>(other_vars_map));
      returning.emplace_back(sub_map);
    }
  }
  return returning;
}

}  // namespace spn::utils

#endif
