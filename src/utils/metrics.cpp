#include "utils/metrics.h"
#include <cmath>
#include <cstdint>
#include <vector>

using std::vector;

namespace spn::utils {

uint64_t hamming(const vector<uint64_t> &p1, const vector<uint64_t> &p2) {
  // By definition p1.size() == p2.size()
  size_t n = p1.size();
  uint64_t s = 0;
  for (size_t i = 0; i < n; i++) {
    if (p1[i] != p2[i]) {
      s++;
    }
  }
  return s;
}

double euclidean(const vector<uint64_t> &p1, const vector<uint64_t> &p2) {
  // By definition p1.size() == p2.size()
  uint64_t n = p1.size();
  double s = 0.0;
  for (uint64_t i = 0; i < n; i++) {
    auto square_root =
        static_cast<double>(static_cast<int64_t>(p1[i]) - static_cast<int64_t>(p2[i]));
    s += square_root * square_root;
  }
  return sqrt(s);
}

}  // namespace spn::utils
