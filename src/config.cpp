#include "config.h"

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <iostream>
#include <string>

namespace spn {

using boost::property_tree::ptree;

Config::Config() : _pt() {
  const std::string CONFIG_PATH = "config/spn.json";
  try {
    read_json(CONFIG_PATH, _pt);
  } catch (const boost::property_tree::json_parser_error &e){
    std::cerr << "Error reading json file: " << CONFIG_PATH << ". The boost error message is as follows:\n" << std::endl;
    std::cerr << e.what() << '\n' << std::endl;
    std::cerr << "Exiting..." << std::endl;
    std::exit(EXIT_FAILURE);
  }
}

const ptree &Config::node(const std::string &key) const { return _pt.get_child(key); }

}  // namespace spn
