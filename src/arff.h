#ifndef _SPN_ARFF_H_
#define _SPN_ARFF_H_

#include "data.h"

#include <fstream>
#include <string>
#include <vector>

namespace spn {

class Attribute {
 public:
  Attribute(std::string name, std::string datatype)
      : _name(move(name)), _datatype(move(datatype)), _variations(), _date_format(){};
  Attribute(std::string name, std::string datatype, std::vector<std::string> variations)
      : _name(move(name)),
        _datatype(move(datatype)),
        _variations(move(variations)),
        _date_format(){};
  Attribute(std::string name, std::string datatype, std::string date_format)
      : _name(move(name)),
        _datatype(move(datatype)),
        _variations(),
        _date_format(move(date_format)){};

  std::vector<std::string> variations() const { return _variations; };
  std::vector<std::string> &variations() { return _variations; };
  std::string datatype() const { return _datatype; };

 private:
  std::string _name;
  std::string _datatype;
  std::vector<std::string> _variations;
  std::string _date_format;
};

class Arff {
 public:
  explicit Arff(std::string filename)
      : _filename(move(filename)), _relation_name(), _attributes(), _data() {
    read_file();
  };

  std::string relation_name() const { return _relation_name; };
  std::vector<Attribute> attributes() const { return _attributes; };
  std::vector<Attribute> attributes() { return _attributes; };
  std::vector<std::vector<std::string>> data() const { return _data; };
  Data to_data() const;

 private:
  void read_file();
  std::string read_relation_name(std::ifstream &file) const;
  std::vector<Attribute> read_attributes(std::ifstream &file) const;
  Attribute read_attribute(std::ifstream &file) const;
  std::vector<std::vector<std::string>> read_data(std::ifstream &file) const;
  void fill_attribute_variations();

  const std::string _filename;
  std::string _relation_name;
  std::vector<Attribute> _attributes;
  std::vector<std::vector<std::string>> _data;
};
}  // namespace spn

#endif
