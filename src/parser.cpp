#include "parser.h"
#include "log.h"
#include "utils/random.h"
#include "variable.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <unordered_map>
#include <vector>

using std::string;
using std::stringstream;
using std::unordered_map;
using std::vector;

namespace spn {

Data parse_data(const char *filename) {
  std::ifstream file(filename, std::ios::in);
  if (!file) {
    logger() << "Error opening file " << filename << "." << std::endl;
    std::exit(EXIT_FAILURE);
  }

  vector<DataVariable> scope;
  string line;

  std::getline(file, line);
  // Each line declaring a variable starts with 'var' and has two integer
  // numbers after it
  for (; line.front() == 'v'; std::getline(file, line)) {
    stringstream line_stream(line);
    string var;  // Unused variable to hold the 'var' string
    uint64_t var_id, n_categories;
    line_stream >> var >> var_id >> n_categories;
    scope.emplace_back(DataVariable{var_id, n_categories});
  }

  uint64_t number_of_variables = scope.size();
  vector<unordered_map<uint64_t, uint64_t>> cvntmap;

  for (size_t i = 0; !file.eof(); std::getline(file, line), i++) {
    stringstream line_stream(line);
    cvntmap.emplace_back(unordered_map<uint64_t, uint64_t>(number_of_variables));
    for (size_t j = 0; j < number_of_variables; j++) {
      line_stream >> cvntmap[i][j];
    }
  }

  return Data{scope, cvntmap};
}

}  // namespace spn
