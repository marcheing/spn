#ifndef _SPN_CLUSTER_H_
#define _SPN_CLUSTER_H_

#include <queue>
#include <unordered_map>
#include <vector>

namespace spn {

// Density-based spatial clustering of applications with noise
// Parameters:
//   - data is the data matrix;
//   - eps is epsilon maximum distance between density core points;
//   - mp is minimum number of points to be considered core points.
std::vector<std::unordered_map<uint64_t, const std::vector<uint64_t> *>> dbscan(std::vector<std::vector<uint64_t>> data,
                                                                 double eps, size_t mp);

// Port of Object from the original code
struct OpticsObject {
  OpticsObject() : data(){};
  OpticsObject(std::vector<uint64_t> d, bool v, size_t i, double r, double c)
      : data(move(d)), vst(v), index(i), rdist(r), cdist(c){};
  std::vector<uint64_t> data;
  bool vst{};
  size_t index{};
  double rdist{};
  double cdist{};

  bool operator==(const OpticsObject &other) {
    return (this->data == other.data) && (this->vst == other.vst) && (this->index == other.index) &&
           (this->rdist == other.rdist) && (this->cdist == other.cdist);
  }
};

struct OpticsObjectDistance {
  double dist;
  int index;
};

struct OpticsQueueItem {
  OpticsObject *object{nullptr};
  double p{};
  int index{};
};

std::vector<OpticsObject> get_neighbors(const std::vector<OpticsObject> &set, const OpticsObject &o,
                                   double eps);

double get_core_dist(const std::vector<OpticsObject> &neighbors, const OpticsObject &o, size_t mp);

void seeds_update(const std::vector<OpticsObject> &neighbors, OpticsObject *o,
                  std::vector<OpticsQueueItem> *pqueue);

void expand(const std::vector<OpticsObject> &set, OpticsObject *o, double eps, size_t mp,
            std::vector<OpticsObject> *order, std::vector<OpticsQueueItem> *pq);

std::vector<std::unordered_map<uint64_t, const std::vector<uint64_t> *>> extract(const std::vector<OpticsObject> &order,
                                                                  double eps);

// OPTICS - Ordering points to identify the clustering structure (OPTICS)
// OPTICS is similar to DBSCAN with the exception that instead of an epsilon
// to bound the distance between points, OPTICS replaces that epsilon with a
// new epsilon that upper bounds the maximum possible epsilon a DBSCAN would
// take Parameters:
//  - data is data matrix;
//  - eps is a maximum distance between density core points upper bound;
//  - mp is minimum number of points to be considered core point
std::vector<std::unordered_map<uint64_t, const std::vector<uint64_t> *>> optics(std::vector<std::vector<uint64_t>> data,
                                                                 double eps, size_t mp);

void kmedoidinsert(size_t which, std::vector<uint64_t> *means,
                   std::vector<std::unordered_map<uint64_t, std::vector<uint64_t> *>> *clusters,
                   const std::vector<uint64_t> &v, uint64_t i);

void kmedoidremove(size_t which, std::vector<uint64_t> *means,
                   std::vector<std::unordered_map<uint64_t, std::vector<uint64_t> *>> *clusters, uint64_t i);

std::vector<std::unordered_map<uint64_t, const std::vector<uint64_t> *>> kmedoid(
    size_t k, const std::vector<std::vector<uint64_t>> &data);
}  // namespace spn

#endif
