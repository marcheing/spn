#ifndef _SPN_NODE_SPN_H_
#define _SPN_NODE_SPN_H_

#include <cmath>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace spn::node {

using Varset = std::unordered_map<uint64_t, uint64_t>;

struct MAPResult {
  MAPResult() : varset(){};
  MAPResult(Varset vset, double v) : varset(std::move(vset)), value(v){};

  Varset varset;
  double value{0.0};
};

class SPN {
 public:
  SPN() : _children(), _scope(){};
  explicit SPN(std::unordered_set<uint64_t> scope) : _children(), _scope(std::move(scope)){};
  SPN(const SPN &spn) = default;
  SPN(SPN &&spn) = default;
  SPN &operator=(const SPN &) = default;
  SPN &operator=(SPN &&) = default;

  const std::unordered_set<uint64_t> &scope() const { return _scope; };
  virtual std::unordered_map<uint64_t, std::vector<uint64_t>> possible_values() const;
  MAPResult map(const std::string &algorithm, Varset *varset) const;
  std::vector<const SPN *> nodes() const;
  std::vector<const SPN *> leaves() const;

  virtual MAPResult max(Varset *varset) const = 0;
  virtual MAPResult ng(Varset *varset) const = 0;
  virtual MAPResult argmax(Varset *varset) const = 0;
  MAPResult map_kbt(const uint64_t k) const;
  virtual std::vector<Varset> kbt(uint64_t k) const = 0;

  virtual double log_value(const Varset &varset) const = 0;
  virtual double value(const Varset &varset) const = 0;
  virtual std::string type() const = 0;
  virtual uint64_t height() const = 0;
  const std::vector<const SPN *> &children() const { return _children; };
  virtual bool decomposable() const = 0;
  std::vector<const SPN *> topological_order() const;
  virtual double max_by_node(std::unordered_map<const SPN *, Varset> *values) const = 0;

  virtual ~SPN() = 0;

 protected:
  std::vector<const SPN *> _children;
  std::unordered_set<uint64_t> _scope;
};

}  // namespace spn::node

#endif
