#ifndef _SPN_NODE_MULTINOMIAL_H_
#define _SPN_NODE_MULTINOMIAL_H_

#include "node/spn.h"

namespace spn::node {

struct Mode {
  size_t index;
  double val;
};

class Multinomial : public SPN {
 public:
  Multinomial(uint64_t var_id, const std::vector<double> &distribution);
  Multinomial(uint64_t var_id, std::vector<uint64_t> counts);
  Multinomial(const Multinomial &node) = default;
  Multinomial(Multinomial &&node) = default;
  Multinomial &operator=(const Multinomial &) = default;
  Multinomial &operator=(Multinomial &&) = default;

  ~Multinomial() override = default;

  MAPResult max(Varset *varset) const override;
  MAPResult ng(Varset *varset) const override;
  MAPResult argmax(Varset *varset) const override;
  std::vector<Varset> kbt(uint64_t k) const override;

  uint64_t var_id() const { return _var_id; };
  std::unordered_map<uint64_t, std::vector<uint64_t>> possible_values() const override;
  std::string type() const override { return "leaf"; };
  double log_value(const Varset &varset) const override;
  double value(const Varset &varset) const override;
  uint64_t height() const override { return 0; }
  bool decomposable() const override { return true; };
  std::pair<Varset, double> max_assignment() const {
    return {Varset{{_var_id, _mode.index}}, _mode.val};
  };
  double max_by_node(std::unordered_map<const SPN *, Varset> *values) const override;

 private:
  uint64_t _var_id;
  std::vector<double> _probability_distribution;
  Mode _mode;
};

Mode calculate_mode(std::vector<double> probability_distribution);

}  // namespace spn::node

#endif
