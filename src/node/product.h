#ifndef _SPN_NODE_PRODUCT_H_
#define _SPN_NODE_PRODUCT_H_

#include "node/spn.h"

namespace spn::node {

// In order to do the merges for the product node part of the KBT, these
// Varsets need to be iterated in order to be successfully merged into new
// ones. This class makes it easier to separate the counting of the iterators.
class VarsetForKBT {
 public:
  VarsetForKBT(Varset varset, std::vector<Varset> other_varsets, size_t index)
      : _varset(move(varset)), _k_best_varsets(move(other_varsets)), _index(index){};

  bool has_next() const { return _index + 1 < _k_best_varsets.size(); };
  VarsetForKBT next() const {
    return VarsetForKBT(_k_best_varsets[_index + 1], _k_best_varsets, _index + 1);
  };

  Varset &varset() { return _varset; };

 private:
  Varset _varset;
  std::vector<Varset> _k_best_varsets;
  size_t _index;
};

class Product : public SPN {
 public:
  Product() : SPN(){};
  Product(const Product &product) = default;
  Product(Product &&product) = default;
  Product &operator=(const Product &) = default;
  Product &operator=(Product &&) = default;

  std::string type() const override { return "product"; };
  void add_child(const SPN *child);
  double log_value(const Varset &varset) const override;
  double value(const Varset &varset) const override;
  double compute(std::vector<double> children_values) const;
  uint64_t height() const override;
  MAPResult max(Varset *varset) const override;
  MAPResult ng(Varset *varset) const override;
  MAPResult argmax(Varset *varset) const override;
  std::vector<Varset> kbt(uint64_t k) const override;

  bool decomposable() const override;
  double max_by_node(std::unordered_map<const SPN *, Varset> *values) const override;

  ~Product() override {
    for (auto &child : _children) delete child;
  };
};

}  // namespace spn::node

#endif
