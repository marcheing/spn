#include "node/product.h"
#include "utils/map.h"
#include "utils/vector.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <numeric>
#include <queue>

using std::vector;

namespace spn::node {
void Product::add_child(const SPN *child) {
  for (const uint64_t &var : child->scope()) {
    _scope.insert(var);
  }
  _children.push_back(child);
}

double Product::log_value(const Varset &varset) const {
  uint64_t n = _children.size();
  double v = 0.0;
  for (uint64_t i = 0; i < n; i++) {
    double t = _children[i]->log_value(varset);
    v += t;
  }
  return v;
}

double Product::value(const Varset &varset) const {
  double val = 1;
  for (const auto &child : _children) {
    val *= child->value(varset);
  }
  return val;
}

double Product::compute(vector<double> children_values) const {
  return std::accumulate(children_values.begin(), children_values.end(), 0.0);
}

uint64_t Product::height() const {
  uint64_t nc = _children.size();
  if (nc > 0ul) {
    uint64_t v = 0;
    for (uint64_t i = 0; i < nc; i++) {
      uint64_t t = _children[i]->height();
      if (t > v) {
        v = t;
      }
    }
    return v + 1;
  }
  return 0;
}

MAPResult Product::max(Varset *varset) const {
  size_t n = _children.size();
  double v = 0;
  for (size_t i = 0; i < n; i++) {
    MAPResult child_map = _children[i]->max(varset);
    Varset &child_map_varset = child_map.varset;
    v += child_map.value;
    for (auto &var_instantiation : child_map_varset) {
      varset->emplace(var_instantiation.first, var_instantiation.second);
    }
  }
  return MAPResult{*varset, v};
}

MAPResult Product::ng(Varset *varset) const {
  size_t n = _children.size();
  for (size_t i = 0; i < n; i++) {
    MAPResult child_map = _children[i]->max(varset);
    Varset &child_map_varset = child_map.varset;
    varset->insert(child_map_varset.begin(), child_map_varset.end());
  }
  return MAPResult{*varset, log_value(*varset)};
}

MAPResult Product::argmax(Varset *varset) const {
  Varset amax;
  size_t n = _children.size();
  double v = 0.0;

  for (size_t i = 0; i < n; i++) {
    MAPResult child_argmax_result = _children[i]->argmax(varset);
    v += child_argmax_result.value;
    for (auto &var_map : child_argmax_result.varset) {
      amax[var_map.first] = var_map.second;
    }
  }

  return MAPResult{amax, v};
}

bool Product::decomposable() const {
  vector<std::unordered_set<uint64_t>> scopes;
  for (const auto &child : _children) {
    scopes.emplace_back(child->scope());
  }
  for (auto scope : scopes) {
    for (auto second_scope : scopes) {
      if (scope == second_scope) {
        continue;
      }
      vector<uint64_t> intersection;
      std::set_intersection(scope.begin(), scope.end(), second_scope.begin(), second_scope.end(),
                            std::back_inserter(intersection));
      if (!intersection.empty()) {
        return false;
      }
    }
  }
  return true;
}

Varset merge_kbt_varsets(VarsetForKBT v1, VarsetForKBT v2) {
  Varset resulting_varset;
  for (auto &var_value : v1.varset()) {
    resulting_varset[var_value.first] = var_value.second;
  }
  for (auto &var_value : v2.varset()) {
    resulting_varset[var_value.first] = var_value.second;
  }
  return resulting_varset;
}

void add_varsets_to_solution_vector(vector<Varset> &solution, VarsetForKBT v1, VarsetForKBT v2) {
  Varset merged_varsets = merge_kbt_varsets(v1, v2);
  if (std::find(solution.begin(), solution.end(), merged_varsets) == solution.end()) {
    solution.emplace_back(merged_varsets);
  }
}

vector<Varset> perform_pops_varset(std::queue<std::pair<VarsetForKBT, VarsetForKBT>> &queue,
                                   uint64_t k) {
  vector<Varset> resulting_varsets;
  for (size_t i = 0; i < k; i++) {
    std::pair<VarsetForKBT, VarsetForKBT> pair_of_varsets = queue.front();
    queue.pop();
    add_varsets_to_solution_vector(resulting_varsets, pair_of_varsets.first,
                                   pair_of_varsets.second);
    if (pair_of_varsets.first.has_next()) {
      std::pair<VarsetForKBT, VarsetForKBT> change_the_first =
          std::make_pair(pair_of_varsets.first.next(), pair_of_varsets.second);
      queue.push(change_the_first);
      add_varsets_to_solution_vector(resulting_varsets, change_the_first.first,
                                     change_the_first.second);
    }
    if (pair_of_varsets.second.has_next()) {
      std::pair<VarsetForKBT, VarsetForKBT> change_the_second =
          std::make_pair(pair_of_varsets.first, pair_of_varsets.second.next());
      queue.push(change_the_second);
      add_varsets_to_solution_vector(resulting_varsets, change_the_second.first,
                                     change_the_second.second);
    }
  }
  return resulting_varsets;
}
vector<Varset> perform_merges_varset(vector<vector<Varset>> &lists_of_varsets, uint64_t k) {
  vector<Varset> &first_list_of_varsets = lists_of_varsets.front();
  vector<Varset> main_list_of_varsets = first_list_of_varsets;
  for (auto lists_of_varsets_iterator = lists_of_varsets.begin() + 1;
       lists_of_varsets_iterator != lists_of_varsets.end(); lists_of_varsets_iterator++) {
    std::queue<std::pair<VarsetForKBT, VarsetForKBT>> queue;
    vector<Varset> &list_to_merge = *lists_of_varsets_iterator;
    VarsetForKBT ev1 = VarsetForKBT(main_list_of_varsets[0], main_list_of_varsets, 0);
    VarsetForKBT ev2 = VarsetForKBT(list_to_merge[0], list_to_merge, 0);
    queue.push(std::make_pair(ev1, ev2));
    main_list_of_varsets = perform_pops_varset(queue, k);
  }
  return main_list_of_varsets;
}

vector<Varset> Product::kbt(uint64_t k) const {
  vector<vector<Varset>> child_kbts;
  for (const auto &child : _children) {
    child_kbts.emplace_back(child->kbt(k));
  }
  vector<Varset> kbts = perform_merges_varset(child_kbts, k);
  utils::inplace_best_k(kbts, k, [this](const Varset &a, const Varset &b) {
    return this->log_value(a) > this->log_value(b);
  });
  return kbts;
}

double Product::max_by_node(std::unordered_map<const SPN *, Varset> *values) const {
  double value = 1.0;
  for (const auto &child : _children) {
    value *= child->max_by_node(values);
  }
  return value;
}

}  // namespace spn::node
