#include "node/sum.h"
#include "utils/map.h"
#include "utils/vector.h"

#include <algorithm>
#include <iostream>
#include <limits>
#include <queue>

using std::unordered_map;
using std::vector;

namespace spn::node {
void Sum::add_child_with_weight(const SPN *child, double weight) {
  for (const uint64_t &var : child->scope()) {
    _scope.insert(var);
  }
  _children.push_back(child);
  _weights.push_back(weight);
}

double Sum::log_value(const Varset &varset) const {
  // This is an alternative way of computing the weighted sum coming
  // from the original implementation by Poon and Domingos.
  uint64_t children_size = _children.size();
  vector<double> vals = vector<double>(children_size);
  for (uint64_t i = 0; i < children_size; i++) {
    double child_value = _children[i]->log_value(varset);
    vals[i] = child_value + log(_weights[i]);
  }
  return compute(vals);
}

double Sum::value(const Varset &varset) const {
  double val = 0.0;
  for (size_t i = 0; i < _children.size(); i++) {
    val += _children[i]->value(varset) * _weights[i];
  }
  return val;
}

double Sum::compute(const vector<double> &children_values) const {
  size_t max_value_index = std::numeric_limits<size_t>::max();
  double max_value = -std::numeric_limits<double>::infinity();
  for (uint64_t i = 0; i < children_values.size(); i++) {
    double value = children_values[i];
    if (value > max_value) {
      max_value = value;
      max_value_index = i;
    }
  }
  if (max_value == -std::numeric_limits<double>::infinity() ||
      max_value == std::numeric_limits<double>::infinity()) {
    return max_value;
  }
  double result = 0.0;
  for (uint64_t i = 0; i < children_values.size(); i++) {
    if (i != max_value_index) {
      result += exp(children_values[i] - max_value);
    }
  }
  return max_value + log1p(result);
}

uint64_t Sum::height() const {
  uint64_t nc = _children.size();
  if (nc > 0ul) {
    uint64_t v = 0;
    for (uint64_t i = 0; i < nc; i++) {
      uint64_t t = _children[i]->height();
      if (t > v) {
        v = t;
      }
    }
    return v + 1;
  }
  return 0;
}

MAPResult Sum::max(Varset *varset) const {
  double max = -std::numeric_limits<double>::infinity();
  size_t n = _children.size();
  Varset max_varset;

  for (size_t i = 0; i < n; i++) {
    MAPResult child_map = _children[i]->max(varset);
    Varset &child_map_varset = child_map.varset;
    double cv = log(_weights[i]) + child_map.value;
    if (cv > max) {
      max = cv;
      max_varset = child_map_varset;
    }
  }
  for (auto &varset_instantiation : max_varset) {
    varset->emplace(varset_instantiation.first, varset_instantiation.second);
  }
  return MAPResult{*varset, max};
}

MAPResult Sum::ng(Varset *varset) const {
  size_t n = _children.size();
  double total_value = 0.0;
  Varset max_varset;

  for (size_t i = 0; i < n; i++) {
    MAPResult child_map = _children[i]->max(varset);
    Varset &child_map_varset = child_map.varset;
    double cv = log(_weights[i]) + child_map.value;
    total_value += cv;
    for (auto &varset_instantiation : child_map_varset) {
      max_varset[varset_instantiation.first] = varset_instantiation.second;
    }
  }
  return MAPResult{max_varset, total_value};
}

MAPResult Sum::argmax(Varset *varset) const {
  size_t n = _children.size();
  double max = -std::numeric_limits<double>::infinity();
  const SPN *mch;

  for (size_t i = 0; i < n; i++) {
    const SPN *child = _children[i];
    double m = log(_weights[i]) + child->argmax(varset).value;
    if (m > max) {
      max = m;
      mch = child;
    }
  }

  MAPResult max_child_result = mch->argmax(varset);
  return MAPResult{max_child_result.varset, max};
}

bool Sum::decomposable() const {
  for (const auto &child : _children) {
    if (!child->decomposable()) {
      return false;
    }
  }
  return true;
}

vector<Varset> Sum::kbt(uint64_t k) const {
  vector<Varset> kbts;
  unordered_map<const SPN *, vector<Varset>> child_kbts;
  unordered_map<const SPN *, size_t> child_kbts_indexes;
  std::queue<std::pair<const SPN *, Varset>> priority_queue;
  for (const auto &child : _children) {
    vector<Varset> child_kbt = child->kbt(k);
    child_kbts[child] = child_kbt;
    child_kbts_indexes[child] = 0;
  }
  for (auto &child_vector_of_kbts : child_kbts) {
    Varset best_tree = child_vector_of_kbts.second.front();
    priority_queue.push(std::make_pair(child_vector_of_kbts.first, best_tree));
    kbts.emplace_back(best_tree);
    child_kbts_indexes[child_vector_of_kbts.first]++;
  }
  for (size_t i = 0; i < k; i++) {
    std::pair<const SPN *, Varset> val = priority_queue.front();
    priority_queue.pop();
    const SPN *child = val.first;
    size_t child_index = child_kbts_indexes[child];
    if (child_index < child_kbts[child].size()) {
      Varset next_best_tree = child_kbts[child][child_index];
      priority_queue.push(std::make_pair(child, next_best_tree));
      child_kbts_indexes[child]++;
      kbts.emplace_back(next_best_tree);
    }
  }
  utils::inplace_best_k(
      kbts, k, [this](Varset &a, Varset &b) { return this->log_value(a) > this->log_value(b); });
  return kbts;
}

double Sum::max_by_node(unordered_map<const SPN *, Varset> *values) const {
  vector<double> child_values(_children.size());
  for (size_t i = 0; i < _children.size(); i++) {
    child_values[i] = _children[i]->max_by_node(values) * _weights[i];
  }
  return *std::max_element(child_values.begin(), child_values.end());
}

}  // namespace spn::node
