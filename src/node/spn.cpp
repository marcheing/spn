#include "node/spn.h"
#include "node/sum.h"
#include "utils/map.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <stack>
#include <unordered_set>

using std::string;
using std::unordered_map;
using std::unordered_set;
using std::vector;

namespace spn::node {

SPN::~SPN() {}

unordered_map<uint64_t, vector<uint64_t>> SPN::possible_values() const {
  unordered_map<uint64_t, vector<uint64_t>> possible;
  for (const auto &child : children()) {
    unordered_map<uint64_t, vector<uint64_t>> child_possible_values = child->possible_values();
    possible.insert(child_possible_values.begin(), child_possible_values.end());
  }
  return possible;
}

MAPResult SPN::map(const string &algorithm, Varset *varset) const {
  if (algorithm == "best-tree") {
    return max(varset);
  } else if (algorithm == "normalized-greedy-selection") {
    return ng(varset);
  } else if (algorithm == "argmax-product") {
    return argmax(varset);
  }
  return MAPResult{*varset, 1.0};
}

vector<const SPN *> SPN::nodes() const {
  unordered_set<const SPN *> nodes;
  std::stack<const SPN *> node_stack;
  node_stack.push(this);
  while (!node_stack.empty()) {
    const SPN *node = node_stack.top();
    node_stack.pop();
    nodes.insert(node);
    for (const auto &child : node->children()) {
      node_stack.push(child);
    }
  }
  vector<const SPN *> returning_nodes;
  std::copy(nodes.begin(), nodes.end(), std::back_inserter(returning_nodes));
  return returning_nodes;
}

vector<const SPN *> SPN::leaves() const {
  vector<const SPN *> leaves = nodes();
  leaves.erase(std::remove_if(leaves.begin(), leaves.end(),
                              [](const SPN *node) { return node->type() != "leaf"; }),
               leaves.end());
  return leaves;
}

vector<const SPN *> SPN::topological_order() const {
  std::deque<const SPN *> to_evaluate{this};
  vector<const SPN *> evaluated;
  while (!to_evaluate.empty()) {
    const SPN *node = to_evaluate.front();
    to_evaluate.pop_front();
    for (const auto &child : node->children()) {
      if (std::find(evaluated.begin(), evaluated.end(), child) == evaluated.end()) {
        to_evaluate.push_back(child);
      }
    }
    evaluated.emplace_back(node);
  }
  return evaluated;
}

MAPResult SPN::map_kbt(const uint64_t k) const {
  vector<Varset> kbt_result = kbt(k);
  Varset best = kbt_result.front();
  return MAPResult{best, log_value(best)};
}

}  // namespace spn::node
