#ifndef _SPN_NODE_SUM_H_
#define _SPN_NODE_SUM_H_

#include "node/spn.h"

namespace spn::node {

class Sum : public SPN {
 public:
  Sum() : SPN(), _weights(){};
  Sum(const Sum &sum) = default;
  Sum(Sum &&sum) = default;
  Sum &operator=(const Sum &) = default;
  Sum &operator=(Sum &&) = default;

  void add_child_with_weight(const SPN *child, double weight);
  std::string type() const override { return "sum"; };
  std::vector<double> weights() const { return _weights; };
  double log_value(const Varset &varset) const override;
  double value(const Varset &varset) const override;
  double compute(const std::vector<double> &children_values) const;
  uint64_t height() const override;
  MAPResult max(Varset *varset) const override;
  MAPResult ng(Varset *varset) const override;
  MAPResult argmax(Varset *varset) const override;
  std::vector<Varset> kbt(uint64_t k) const override;

  bool decomposable() const override;
  double max_by_node(std::unordered_map<const SPN *, Varset> *values) const override;

  ~Sum() override {
    for (auto &child : _children) delete child;
  };

 private:
  std::vector<double> _weights;
};

}  // namespace spn::node

#endif
