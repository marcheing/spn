#include "node/multinomial.h"
#include "log.h"
#include "utils/map.h"
#include "utils/math.h"
#include "utils/random.h"

#include <gsl/gsl_statistics_double.h>

using std::unordered_map;
using std::vector;

namespace spn::node {

Multinomial::Multinomial(uint64_t var_id, const vector<double> &distribution)
    : SPN({var_id}),
      _var_id(var_id),
      _probability_distribution(distribution),
      _mode(calculate_mode(distribution)) {}

Multinomial::Multinomial(uint64_t var_id, vector<uint64_t> counts)
    : SPN({var_id}), _var_id(var_id), _probability_distribution(), _mode() {
  uint64_t n = counts.size();
  vector<double> probabilities(n);
  double s = 0.0;
  for (uint64_t i = 0; i < n; i++) {
    s += 1.0 + static_cast<double>(counts[i]);
    probabilities[i] = static_cast<double>(1 + counts[i]);
  }

  for (uint64_t i = 0; i < n; i++) {
    probabilities[i] /= s;
  }

  this->_probability_distribution = probabilities;
  this->_mode = calculate_mode(probabilities);
}

unordered_map<uint64_t, vector<uint64_t>> Multinomial::possible_values() const {
  unordered_map<uint64_t, vector<uint64_t>> possible;
  possible[_var_id] = vector<uint64_t>();
  for (size_t i = 0; i < _probability_distribution.size(); i++) {
    possible[_var_id].emplace_back(i);
  }
  return possible;
}

double Multinomial::log_value(const Varset &varset) const {
  double val = value(varset);
  if (val == 0.0) {
    return -std::numeric_limits<double>::infinity();
  }
  return log(value(varset));
}

double Multinomial::value(const Varset &varset) const {
  try {
    return _probability_distribution[static_cast<size_t>(varset.at(_var_id))];
  } catch (std::out_of_range &e) {
    return 0.0;
  }
}

Mode calculate_mode(vector<double> probability_distribution) {
  // FIXME: Code based on the original implementation; Clearly breaks for a
  // vector of probabilities with 3 or more large values before the true mode is
  // found
  uint64_t n = probability_distribution.size();
  double maximum_probability = -1.0;
  vector<size_t> mode_index_values(n);
  size_t mode_index;
  uint64_t number_of_mode_indexes = 0;
  for (uint64_t i = 0; i < n; i++) {
    if (probability_distribution[i] > maximum_probability) {
      maximum_probability = probability_distribution[i];
      mode_index_values[0] = i;
      mode_index = i;
      number_of_mode_indexes = 1;
    } else {
      if (probability_distribution[i] == maximum_probability) {
        mode_index_values[number_of_mode_indexes] = i;
        number_of_mode_indexes++;
      }
    }
  }
  if (number_of_mode_indexes > 1) {
    uint64_t index = utils::generate_random(0ul, mode_index_values.size() - 1);
    mode_index = mode_index_values[index];
  }
  return Mode{mode_index, maximum_probability};
}

MAPResult Multinomial::max(Varset *varset) const {
  try {
    return MAPResult{*varset, log(_probability_distribution[varset->at(_var_id)])};
  } catch (std::out_of_range &e) {
    varset->emplace(_var_id, _mode.index);
    return MAPResult{*varset, log(_mode.val)};
  }
}

MAPResult Multinomial::ng(Varset *varset) const { return max(varset); }

MAPResult Multinomial::argmax(Varset *varset) const {
  Varset retval;
  try {
    uint64_t instantiation = varset->at(_var_id);
    retval[_var_id] = instantiation;
    return MAPResult{retval, log(_probability_distribution[instantiation])};
  } catch (std::out_of_range &e) {
    retval[_var_id] = _mode.index;
    return MAPResult{retval, log(_mode.val)};
  }
}

vector<Varset> Multinomial::kbt(uint64_t k) const {
  vector<std::pair<uint64_t, double>> value_probability_vector;
  size_t number_of_values = _probability_distribution.size();
  vector<Varset> kbest;
  for (size_t i = 0; i < number_of_values; i++) {
    value_probability_vector.emplace_back(std::make_pair(i, _probability_distribution[i]));
  }
  std::sort(value_probability_vector.begin(), value_probability_vector.end(),
            [](const std::pair<uint64_t, double> &a, const std::pair<uint64_t, double> &b) {
              return a.second > b.second;
            });
  for (size_t i = 0; i < k && i < number_of_values; i++) {
    Varset varset;
    varset[_var_id] = value_probability_vector[i].first;
    kbest.emplace_back(varset);
  }
  return kbest;
}

double Multinomial::max_by_node(unordered_map<const SPN *, Varset> *values) const {
  Varset &varset = values->at(this);
  if (utils::has_key(varset, _var_id)) {
    return _probability_distribution.at(varset.at(_var_id));
  }
  varset[_var_id] = _mode.index;
  return _mode.val;
}

}  // namespace spn::node
