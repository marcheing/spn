#ifndef _SPN_DATA_H_
#define _SPN_DATA_H_

#include <string>
#include <unordered_map>
#include <vector>

namespace spn {

class DataVariable {
 public:
  DataVariable(uint64_t index, uint64_t range) : _index(index), _range(range){};

  uint64_t index() const { return _index; };
  uint64_t range() const { return _range; };

 private:
  const uint64_t _index;
  const uint64_t _range;
};

class Data {
 public:
  Data(std::vector<DataVariable> variables, std::vector<std::unordered_map<uint64_t, uint64_t>> instances)
      : _variables(move(variables)), _instances(move(instances)){};

  void write(const std::string &filename) const;
  std::pair<Data, Data> random_partition(const double proportion) const;
  const std::vector<DataVariable> &variables() const { return _variables; };
  const std::vector<std::unordered_map<uint64_t, uint64_t>> &instances() const { return _instances; };

 private:
  std::vector<DataVariable> _variables;
  std::vector<std::unordered_map<uint64_t, uint64_t>> _instances;
};

}  // namespace spn

#endif
