#ifndef _SPN_APP_H_
#define _SPN_APP_H_

#include "config.h"

#include <string>

namespace spn {

class App {
 public:
  App(const App &) = delete;
  App(App &&app) = delete;
  App &operator=(const App &) = delete;
  App &operator=(App &&) = delete;
  ~App() = default;

  const static App &instance() {
    static App app;
    return app;
  }

  void start() const;

 private:
  void init_log() const;
  void execute_actions() const;
  App();

  const std::string _version;
};

}  // namespace spn

#endif
