#include "data.h"
#include "utils/random.h"

#include <fstream>
#include <string>
#include <vector>

using std::pair;
using std::string;
using std::unordered_map;
using std::vector;

namespace spn {

void Data::write(const string &filename) const {
  std::ofstream file(filename, std::ios::out);
  for (DataVariable variable : _variables) {
    file << "var " << variable.index() << " " << variable.range() << std::endl;
  }
  for (const unordered_map<uint64_t, uint64_t> &instance_row : _instances) {
    for (auto &[var, instance] : instance_row) {
      file << instance << " ";
    }
    file << std::endl;
  }
}

std::pair<Data, Data> Data::random_partition(const double proportion) const {
  auto partition_size = static_cast<uint64_t>(proportion * static_cast<double>(_instances.size()));
  vector<unordered_map<uint64_t, uint64_t>> partitioned_instances;
  vector<unordered_map<uint64_t, uint64_t>> other_instances;

  vector<size_t> partition_vector = utils::generate_random_vector_without_repetition<size_t>(
      0, _instances.size() - 1, partition_size);
  for (uint64_t i = 0; i < _instances.size(); i++) {
    if (std::find(partition_vector.begin(), partition_vector.end(), i) != partition_vector.end()) {
      partitioned_instances.emplace_back(_instances[i]);
    } else {
      other_instances.emplace_back(_instances[i]);
    }
  }
  return pair<Data, Data>{Data{_variables, partitioned_instances},
                          Data{_variables, other_instances}};
}

}  // namespace spn
