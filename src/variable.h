#ifndef _SPN_VARIABLE_H_
#define _SPN_VARIABLE_H_

#include <cstdint>

namespace spn {
struct Variable {
  uint64_t var_id;
  uint64_t n_categories;
};
}  // namespace spn

#endif
