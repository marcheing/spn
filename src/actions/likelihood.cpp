#include "actions/base.h"
#include "database/handler.h"

using std::string;

namespace spn::actions {

void likelihood(const ptree &params) {
  auto spn_name = params.get<string>("spn");
  auto dataset_name = params.get<string>("dataset");
  const node::SPN *spn = database::Handler::instance().spn(spn_name);
  const Data &dataset = database::Handler::instance().data(dataset_name);
  logger() << "Running through learned data:" << std::endl;
  for (const auto &d : dataset.instances()) {
    logger() << spn->log_value(d) << std::endl;
  }
}

}  // namespace spn::actions
