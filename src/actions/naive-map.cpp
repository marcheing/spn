#include "actions/base.h"
#include "database/handler.h"

#include <cmath>
#include <stack>
#include <vector>

using std::endl;
using std::stack;
using std::string;
using std::unordered_map;
using std::unordered_set;
using std::vector;

namespace spn::actions {

vector<node::Varset> all_possible_varsets(const node::SPN *spn) {
  vector<node::Varset> varsets;
  unordered_map<uint64_t, vector<uint64_t>> possible_values = spn->possible_values();
  const unordered_set<uint64_t> &variables = spn->scope();
  stack<node::Varset> stack;
  stack.push(node::Varset{});
  for (const uint64_t &var : variables) {
    vector<node::Varset> varsets_with_all_values_of_this_variable;
    while (!stack.empty()) {
      node::Varset current_varset = stack.top();
      stack.pop();
      for (uint64_t &value : possible_values[var]) {
        node::Varset new_varset(current_varset);
        new_varset[var] = value;
        varsets_with_all_values_of_this_variable.emplace_back(new_varset);
      }
    }
    for (node::Varset &varset : varsets_with_all_values_of_this_variable) {
      stack.push(varset);
    }
  }
  while (!stack.empty()) {
    varsets.emplace_back(stack.top());
    stack.pop();
  }
  return varsets;
}

node::MAPResult execute_naive_map(const node::SPN *spn) {
  vector<node::Varset> varsets = all_possible_varsets(spn);
  logger() << "We have " << varsets.size() << " varsets" << '\n';
  node::Varset max_varset;
  double max = -std::numeric_limits<double>::infinity();
  for (node::Varset &varset : varsets) {
    double value = spn->value(varset);
    if (value > max) {
      max = value;
      max_varset = varset;
    }
  }
  return node::MAPResult{max_varset, max};
}

void naive_map(const ptree &params) {
  auto spn_name = params.get<string>("spn");
  const node::SPN *spn = database::Handler::instance().spn(spn_name);
  logger() << "Executing Naive Map on SPN \"" << spn_name << endl;
  node::MAPResult result = execute_naive_map(spn);
  logger() << "  Result: " << endl;
  logger() << "    Value: " << result.value << endl;
  logger() << "    Realized by: ";
  for (auto &instantiation : result.varset) {
    logger() << instantiation.first << "->" << instantiation.second << "   ";
  }
  logger() << endl;
  logger() << "    Proof: " << spn->value(result.varset) << endl;
}

}  // namespace spn::actions
