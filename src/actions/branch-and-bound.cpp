#include "actions/base.h"
#include "database/handler.h"
#include "node/multinomial.h"
#include "node/product.h"
#include "node/sum.h"
#include "utils/log.h"
#include "utils/map.h"
#include "utils/set.h"
#include "utils/vector.h"

#include <cmath>
#include <functional>
#include <iostream>
#include <stack>

using std::unordered_map;
using std::vector;

namespace spn::actions {

class CompleteAssignment {
 public:
  CompleteAssignment() : _assignments(), _value(), _varset(){};
  CompleteAssignment(unordered_map<const node::SPN *, node::Varset> instantiations, double value)
      : _assignments(std::move(instantiations)), _value(value), _varset(){};
  bool feasible();
  const node::Varset &varset() const { return _varset; };
  double value() const { return _value; }

 private:
  unordered_map<const node::SPN *, node::Varset> _assignments;
  double _value{0.0};
  node::Varset _varset;
};

bool CompleteAssignment::feasible() {
  node::Varset comparison_varset;
  for (auto &[_, varset] : _assignments) {
    for (auto &[var, val] : varset) {
      if (utils::has_key(comparison_varset, var)) {
        if (comparison_varset[var] != val) {
          return false;
        }
      } else {
        comparison_varset[var] = val;
      }
    }
  }
  _varset = comparison_varset;
  return true;
}

CompleteAssignment get_upper_bound(const node::SPN *spn, const node::Varset &fixed_variables) {
  vector<const node::SPN *> leaves = spn->leaves();
  unordered_map<const node::SPN *, node::Varset> instantiation;
  for (auto &leaf : leaves) {
    node::Varset copied_varset(fixed_variables);
    instantiation[leaf] = copied_varset;
  }
  double max = spn->max_by_node(&instantiation);
  return CompleteAssignment(instantiation, max);
}

CompleteAssignment get_upper_bound_value(const node::SPN *spn, const node::Varset &varset) {
  vector<const node::SPN *> leaves = spn->leaves();
  unordered_map<const node::SPN *, node::Varset> instantiation;
  for (auto &leaf : leaves) {
    node::Varset copied_varset(varset);
    instantiation[leaf] = copied_varset;
  }
  double max = spn->value(varset);
  return CompleteAssignment(instantiation, max);
}

node::MAPResult execute_branch_and_bound(const node::SPN *spn, uint64_t number_of_fixed_variables) {
  if (number_of_fixed_variables > spn->scope().size()) {
    return {node::Varset(), 0.0};
  }
  unordered_map<uint64_t, vector<uint64_t>> possible_values = spn->possible_values();
  node::Varset current_best;
  vector<uint64_t> var_vector = utils::to_vector(spn->scope());
  vector<vector<uint64_t>> vars_to_fix = utils::combine_vector<uint64_t>(
      var_vector.begin(), var_vector.end(), number_of_fixed_variables);
  for (auto &segment : vars_to_fix) {
    utils::log_vector(segment);
  }
  for (auto &vars : vars_to_fix) {
    vector<node::Varset> fixed_varsets =
        utils::combine_map<uint64_t, uint64_t, uint64_t>(vars.begin(), vars.end(), possible_values);
    for (auto &varset : fixed_varsets) {
      varset.merge(current_best);
    }
    for (auto &varset : fixed_varsets) {
      utils::log_map(varset);
    }
    vector<std::pair<CompleteAssignment, node::Varset>> assignments(fixed_varsets.size());
    if (number_of_fixed_variables == spn->scope().size()) {
      std::transform(fixed_varsets.begin(), fixed_varsets.end(), assignments.begin(),
                     [spn](const node::Varset &varset) {
                       return std::make_pair(get_upper_bound_value(spn, varset), varset);
                     });
    } else {
      std::transform(fixed_varsets.begin(), fixed_varsets.end(), assignments.begin(),
                     [spn](const node::Varset &varset) {
                       return std::make_pair(get_upper_bound(spn, varset), varset);
                     });
    }
    auto best_assignment_it =
        std::max_element(assignments.begin(), assignments.end(),
                         [](const std::pair<CompleteAssignment, node::Varset> &a1,
                            const std::pair<CompleteAssignment, node::Varset> &a2) {
                           return a1.first.value() < a2.first.value();
                         });
    int64_t number_of_best_assignments =
        std::count_if(assignments.begin(), assignments.end(),
                      [best_assignment_it](const std::pair<CompleteAssignment, node::Varset> &a) {
                        return a.first.value() == ((*best_assignment_it).first.value());
                      });
    vector<double> values(assignments.size());
    std::transform(
        assignments.begin(), assignments.end(), values.begin(),
        [](const std::pair<CompleteAssignment, node::Varset> a) { return a.first.value(); });
    utils::log_vector(values);
    if (number_of_best_assignments > 1) {
      if (vars == vars_to_fix.back()) {
        return execute_branch_and_bound(spn, number_of_fixed_variables + 1);
      }
      continue;
    }
    std::pair<CompleteAssignment, node::Varset> &best_assignment = *best_assignment_it;
    if (best_assignment.first.feasible()) {
      return node::MAPResult{best_assignment.first.varset(), best_assignment.first.value()};
    }
    current_best = best_assignment.second;
  }
  return {node::Varset(), 0.0};
}  // namespace spn::actions

void branch_and_bound(const ptree &params) {
  auto spn_name = params.get<std::string>("spn");
  const node::SPN *spn = database::Handler::instance().spn(spn_name);
  logger() << "Executing B&B on SPN \"" << spn_name << std::endl;
  node::MAPResult result = execute_branch_and_bound(spn, 1);
  logger() << "  Result: " << std::endl;
  logger() << "    Log-value: " << result.value << std::endl;
  logger() << "    Value: " << exp(result.value) << std::endl;
  logger() << "    Realized by: ";
  for (auto &instantiation : result.varset) {
    logger() << instantiation.first << "->" << instantiation.second << "   ";
  }
  logger() << std::endl;
  logger() << "    Proof: " << spn->value(result.varset) << std::endl;
}

}  // namespace spn::actions
