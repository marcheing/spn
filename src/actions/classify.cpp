#include "actions/base.h"
#include "data.h"
#include "database/handler.h"

#include <algorithm>
#include <cmath>
#include <iostream>
#include <limits>
#include <numeric>
#include <unordered_map>

using std::string;
using std::unordered_map;
using std::vector;

namespace spn::actions {

uint64_t most_probable_value_for_var(const node::SPN *const spn, node::Varset *const varset,
                                     const DataVariable &var) {
  const uint64_t var_index = var.index();
  const uint64_t var_n_values = var.range();
  vector<uint64_t> var_values(var_n_values);
  std::iota(var_values.begin(), var_values.end(), 0);
  vector<double> probabilities(var_n_values);
  for (auto &value : var_values) {
    varset->at(var_index) = value;
    probabilities[value] = spn->log_value(*varset);
  }
  const auto max_probability_iterator =
      std::max_element(probabilities.begin(), probabilities.end());
  return static_cast<uint64_t>(std::distance(probabilities.begin(), max_probability_iterator));
}

void classify(const ptree &params) {
  const auto spn_name = params.get<string>("spn");
  const auto dataset_name = params.get<string>("dataset");
  const node::SPN *spn1 = database::Handler::instance().spn(spn_name);
  const Data &data = database::Handler::instance().data(dataset_name);

  unordered_map<string, const node::SPN *> spns;
  spns["SPN1"] = spn1;

  unordered_map<string, uint64_t> corrects_count;
  corrects_count["SPN1"] = 0;

  const uint64_t lines = data.instances().size();
  const size_t n = data.variables().size();

  uint64_t corrects = 0;

  for (const auto &[name, spn] : spns) {
    vector<node::Varset> test = data.instances();
    corrects = 0;
    for (uint64_t i = 0; i < lines; i++) {
      const uint64_t value_that_should_be =
          data.instances()[i].at(data.variables().at(n - 1).index());
      logger() << name << ": Testing instance " << i << " and should be classified as "
               << value_that_should_be << std::endl;
      uint64_t most_probable_value =
          most_probable_value_for_var(spn, &test[i], data.variables().at(n - 1));
      test[i][n - 1] = std::numeric_limits<uint64_t>::max();

      logger() << name << ": Instance " << i << " should be classified as " << value_that_should_be
               << " SPN classified as " << most_probable_value << " ok = "
               << static_cast<int64_t>(most_probable_value) -
                      static_cast<int64_t>(value_that_should_be)
               << std::endl;
      if (most_probable_value == value_that_should_be) {
        corrects++;
      } else {
        logger() << "--------> INCORRECT! <--------" << std::endl;
      }
      test[i].erase(n - 1);
    }

    logger() << "========= Iteration Results ========" << std::endl;
    logger() << "  " << name << ": Correct classifications: " << corrects << "/" << lines
             << std::endl;
    logger() << "  Percentage of correct hits: "
             << 100.0 * (static_cast<double>(corrects) / static_cast<double>(lines)) << "%%"
             << std::endl;
    logger() << "  Test set size: " << test.size() << std::endl;
    logger() << "======================================" << std::endl;

    corrects_count[name] = corrects;
  }
}

}  // namespace spn::actions
