#include "base.h"

using std::function;
using std::string;
using std::unordered_map;

namespace spn::actions {

const function<void(const ptree &)> action_named(const string &name) {
  const static unordered_map<string, function<void(const ptree &)>> action_map{
      {"beam-search", beam_search},
      {"branch-and-bound", branch_and_bound},
      {"classify", classify},
      {"convert", convert},
      {"kbt", kbt},
      {"likelihood", likelihood},
      {"load-data", load_data},
      {"map", map},
      {"mis", mis},
      {"mis-spn", mis_spn},
      {"naive-map", naive_map},
      {"partition", partition},
      {"print", print},
      {"spn", spn}};
  return action_map.at(name);
}

}  // namespace spn::actions
