#include "actions/base.h"
#include "arff.h"
#include "data.h"

using std::string;

namespace spn::actions {

void convert(const ptree &params) {
  auto input = params.get<string>("input");
  auto output = params.get<string>("output");
  logger() << "Opening file " << input << std::endl;
  Arff arff(input);
  logger() << "Transforming file into data format" << std::endl;
  Data data = arff.to_data();
  logger() << "Writing data file into " << output << std::endl;
  data.write(output);
}

}  // namespace spn::actions
