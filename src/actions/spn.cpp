#include "actions/base.h"
#include "data.h"
#include "database/handler.h"
#include "learn/gens.h"
#include "parser.h"

#include <memory>

using std::endl;
using std::string;
using std::unordered_map;
using std::vector;

namespace spn::actions {

void spn(const ptree &params) {
  auto name = params.get<string>("name");
  auto dataset = params.get<string>("dataset");
  auto kclusters = params.get<int>("kclusters");
  auto pval = params.get<double>("pval");
  auto eps = params.get<double>("eps");
  auto mp = params.get<size_t>("mp");

  const Data &data = database::Handler::instance().data(dataset);
  unordered_map<uint64_t, Variable> scope(data.variables().size());
  for (const DataVariable &data_var : data.variables()) {
    scope[data_var.index()] = Variable{data_var.index(), data_var.range()};
  }
  const node::SPN *spn = learn::gens(scope, data.instances(), kclusters, pval, eps, mp);
  database::Handler::instance().insert(name, spn);
  logger() << "SPN \"" << name << "\" is ready" << endl;
  logger() << "  Scope: ";
  for (auto &var_name : spn->scope()) {
    logger() << var_name << " ";
  }
  logger() << endl;
  logger() << "  Possible values: " << endl;
  unordered_map<uint64_t, vector<uint64_t>> possible_values = spn->possible_values();
  for (uint64_t var = 0; var < possible_values.size(); var++) {
    logger() << "    " << var << ": ";
    for (auto &possible_value : possible_values[var]) {
      logger() << possible_value << ' ';
    }
    logger() << endl;
  }
}

}  // namespace spn::actions
