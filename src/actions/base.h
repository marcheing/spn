#ifndef _SPN_ACTION_BASE_H_
#define _SPN_ACTION_BASE_H_

#include "log.h"

#include <boost/property_tree/ptree.hpp>
#include <functional>
#include <string>
#include <unordered_map>

using std::function;
using std::string;

namespace spn::actions {

using boost::property_tree::ptree;

void beam_search(const ptree &params);
void branch_and_bound(const ptree &params);
void classify(const ptree &params);
void convert(const ptree &params);
void kbt(const ptree &params);
void likelihood(const ptree &params);
void load_data(const ptree &params);
void map(const ptree &params);
void mis(const ptree &params);
void mis_spn(const ptree &params);
void naive_map(const ptree &params);
void partition(const ptree &params);
void print(const ptree &params);
void spn(const ptree &params);

const function<void(const ptree &)> action_named(const string &name);

}  // namespace spn::actions

#endif
