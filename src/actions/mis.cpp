#include "actions/base.h"
#include "database/handler.h"
#include "graph/mis-instance.h"

using std::string;

namespace spn::actions {

void mis(const ptree &params) {
  auto filename = params.get<string>("filename");
  auto instance_name = params.get<string>("name");
  logger() << "Opening file " << filename << std::endl;
  const graph::MISInstance graph(filename.c_str());
  logger() << "The graph has " << graph.n_vertices() << " vertices and " << graph.n_edges()
           << " edges." << std::endl;
  database::Handler::instance().insert(instance_name, graph);
  logger() << "Inserted " << instance_name << " on database" << std::endl;
}

}  // namespace spn::actions
