#include "actions/base.h"
#include "database/handler.h"

using std::string;

namespace spn::actions {

void mis_spn(const ptree &params) {
  auto spn_name = params.get<string>("name");
  auto mis_instance_name = params.get<string>("mis");
  logger() << "Creating SPN based on mis instance " << mis_instance_name << std::endl;
  const graph::MISInstance &mis_instance =
      database::Handler::instance().mis_instance(mis_instance_name);
  const node::SPN *spn = mis_instance.to_spn();
  logger() << "Inserting SPN " << spn_name << " on database" << std::endl;
  database::Handler::instance().insert(spn_name, spn);
}

}  // namespace spn::actions
