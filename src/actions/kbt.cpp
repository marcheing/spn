#include "actions/base.h"
#include "database/handler.h"

#include <cmath>

using std::string;

namespace spn::actions {

void kbt(const ptree &params) {
  auto spn_name = params.get<string>("spn");
  auto k = params.get<uint64_t>("k");
  const node::SPN *spn = database::Handler::instance().spn(spn_name);
  logger() << "Executing K-Best Trees on SPN \"" << spn_name << "\" with k = \"" << k << '\"'
           << std::endl;
  node::MAPResult result = spn->map_kbt(k);
  logger() << "  Result: " << std::endl;
  logger() << "    Log-value: " << result.value << std::endl;
  logger() << "    Value: " << exp(result.value) << std::endl;
  logger() << "    Realized by: ";
  for (auto &instantiation : result.varset) {
    logger() << instantiation.first << "->" << instantiation.second << "   ";
  }
  logger() << std::endl;
  logger() << "    Proof: " << spn->value(result.varset) << std::endl;
}

}  // namespace spn::actions
