#include "actions/base.h"
#include "database/handler.h"
#include "node/multinomial.h"
#include "node/sum.h"

#include <deque>
#include <queue>
#include <sstream>

using std::string;

namespace spn::actions {

struct BFSPair {
  BFSPair(const BFSPair &) = default;
  BFSPair &operator=(const BFSPair &) = default;
  BFSPair(BFSPair &&) = default;
  BFSPair &operator=(BFSPair &&) = default;

  ~BFSPair() = default;

  explicit BFSPair(const node::SPN *spn, string pname, double weight)
      : _spn(spn), _pname(move(pname)), _weight(weight){};

  const node::SPN *_spn;
  string _pname;
  double _weight;
};

void print(const ptree &params) {
  auto spn_name = params.get<string>("spn");
  auto filename = params.get<string>("filename");
  const node::SPN *node = database::Handler::instance().spn(spn_name);

  std::ofstream file(filename, std::ios::binary);
  file << "graph {\n";
  if (node->type() == "leaf") {
    file << "X1 [label=<X<sub>1</sub>>,shape=circle];\n}";
    return;
  }
  uint64_t nvars = 0, nsums = 0, nprods = 0;
  std::queue<BFSPair *> queue;
  queue.push(new BFSPair(node, string(), -1.0));
  while (!queue.empty()) {
    BFSPair *current_pair = queue.front();
    queue.pop();
    const node::SPN *current = current_pair->_spn;
    string pname = current_pair->_pname;
    double pw = current_pair->_weight;
    const std::vector<const node::SPN *> &children = current->children();
    size_t number_of_children = node->children().size();
    string name = "N";
    std::ostringstream name_stream;
    string current_type = current->type();

    // In case it is a sum node. Else product node.
    if (current_type == "sum") {
      name_stream << 'S' << nsums;
      name = name_stream.str();
      file << name << " [label=\"+\",shape=circle];\n";
      nsums++;
    } else if (current_type == "product") {
      name_stream << "P" << nprods;
      name = name_stream.str();
      file << name << " [label=<&times;>,shape=circle];\n";
      nprods++;
    }

    // If pname is empty, then it is the root node. Else, link
    // parent node to current node.
    if (!pname.empty()) {
      if (pw >= 0.0) {
        file << pname << " -- " << name << " [label=\"" << pw << "\"];\n";
      } else {
        file << pname << " -- " << name << '\n';
      }
    }

    std::vector<double> w;
    if (current_type == "sum") {
      w = dynamic_cast<const node::Sum *>(current)->weights();
    }
    // For each children, run the BFS.
    for (size_t i = 0; i < number_of_children; i++) {
      const node::SPN *child = children[i];

      // If leaf, then simply write to the graphviz dot file. Else, recurse the
      // BFS
      if (child->type() == "leaf") {
        string cname;
        std::ostringstream cname_stream;
        cname_stream << 'X' << nvars;
        cname = cname_stream.str();
        file << cname << " [label=<X<sub>"
             << dynamic_cast<const node::Multinomial *>(child)->var_id()
             << "</sub>>,shape=circle];\n";
        nvars++;

        if (current_type == "sum") {
          file << name << " -- " << cname << " [label=\"" << w[i] << "\"]\n";
        } else {
          file << name << " -- " << cname << '\n';
        }
      } else {
        double tw = -1.0;
        if (!w.empty()) {
          tw = w[i];
        }
        queue.push(new BFSPair(child, name, tw));
      }
    }
    delete current_pair;
  }
  file << '}';
}

}  // namespace spn::actions
