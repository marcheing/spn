#include "actions/base.h"
#include "database/handler.h"

#include <cmath>

using std::string;
using std::unordered_map;
using std::unordered_set;
using std::vector;

namespace spn::actions {

node::MAPResult execute_beam_search(const uint64_t beam_size, const node::SPN *spn) {
  node::Varset sample;
  unordered_map<uint64_t, vector<uint64_t>> var_values = spn->possible_values();
  const unordered_set<uint64_t> &variable_scope = spn->scope();
  for (auto &variable : variable_scope) {
    sample[variable] = var_values[variable][0];
  }
  vector<std::pair<node::Varset, double>> samples_and_values;
  double current_result = spn->log_value(sample);
  samples_and_values.emplace_back(make_pair(sample, current_result));
  for (auto &current_var_being_modified : variable_scope) {
    size_t current_number_of_solutions = samples_and_values.size();
    for (uint64_t i = 0; i < current_number_of_solutions; i++) {
      node::Varset current_varset = samples_and_values[i].first;
      for (auto &possible_value : var_values[current_var_being_modified]) {
        if (possible_value == samples_and_values[i].first[current_var_being_modified]) {
          continue;  // We'll not repeat any values
        }
        node::Varset new_varset(current_varset);
        new_varset[current_var_being_modified] = possible_value;
        double new_result = spn->log_value(new_varset);
        samples_and_values.emplace_back(make_pair(new_varset, new_result));
      }
    }
    std::sort(samples_and_values.begin(), samples_and_values.end(),
              [](std::pair<node::Varset, double> &a, std::pair<node::Varset, double> &b) {
                return a.second > b.second;
              });
    if (samples_and_values.size() > beam_size) {
      samples_and_values.erase(samples_and_values.begin() + static_cast<int64_t>(beam_size),
                               samples_and_values.end());
    }
  }
  return node::MAPResult{samples_and_values[0].first, samples_and_values[0].second};
}

void beam_search(const ptree &params) {
  auto spn_name = params.get<string>("spn");
  auto beam_size = params.get<uint64_t>("beam-size");
  const node::SPN *spn = database::Handler::instance().spn(spn_name);
  logger() << "Executing Beam Search on SPN \"" << spn_name << "\" with beam size \"" << beam_size
           << '\"' << std::endl;
  node::MAPResult result = execute_beam_search(beam_size, spn);
  logger() << "  Result: " << std::endl;
  logger() << "    Log-value: " << result.value << std::endl;
  logger() << "    Value: " << exp(result.value) << std::endl;
  logger() << "    Realized by: ";
  for (auto &instantiation : result.varset) {
    logger() << instantiation.first << "->" << instantiation.second << "   ";
  }
  logger() << std::endl;
  logger() << "    Proof: " << spn->value(result.varset) << std::endl;
}

}  // namespace spn::actions
