#include "actions/base.h"
#include "database/handler.h"
#include "parser.h"

using std::string;

namespace spn::actions {

void load_data(const ptree &params) {
  const string &filename = params.get<string>("filename");
  const string &name = params.get<string>("name");
  logger() << "Executing Load Data on file \"" << filename << "\" and storing it as \"" << name
           << "\"\n";
  const Data data = parse_data(filename.c_str());
  database::Handler::instance().insert(name, data);
}

}  // namespace spn::actions
