#include "actions/base.h"
#include "database/handler.h"
#include "utils/math.h"
#include "utils/random.h"

#include <cmath>

using std::string;
using std::unordered_map;
using std::vector;

namespace spn::actions {

void map(const ptree &params) {
  auto spn_name = params.get<string>("spn");
  auto dataset_name = params.get<string>("dataset");
  auto algorithm = params.get<string>("algorithm");
  auto proportion = params.get<double>("proportion");

  const node::SPN *spn = database::Handler::instance().spn(spn_name);
  const Data &dataset = database::Handler::instance().data(dataset_name);
  logger() << "Executing MAP on SPN \"" << spn_name << "\" using the algorithm \"" << algorithm
           << '\"' << std::endl;
  auto n_vars =
      static_cast<size_t>(proportion * static_cast<double>(dataset.instances().at(0).size()));
  logger() << "  Using exactly " << n_vars << " variables" << std::endl;
  vector<uint64_t> variables;
  unordered_map<uint64_t, vector<uint64_t>> possible_values = spn->possible_values();
  uint64_t total_spn_variables = possible_values.size();
  for (size_t i = 0; i < total_spn_variables; i++) {
    variables.emplace_back(i);
  }

  vector<uint64_t> selected_variables = utils::random_sample<uint64_t>(n_vars, variables);
  node::Varset varset;

  for (uint64_t variable : selected_variables) {
    varset[variable] = utils::random_element<uint64_t>(possible_values[variable]);
  }

  if (varset.empty()) {
    logger() << "  With an empty instantiation" << std::endl;
  } else {
    logger() << "  With the following instantiation: " << std::endl;
    logger() << "  ";
    for (auto &key_value : varset) {
      logger() << key_value.first << " -> " << key_value.second << "   ";
    }
    logger() << std::endl;
  }
  node::MAPResult result = spn->map(algorithm, &varset);
  logger() << "  Result: " << std::endl;
  logger() << "    Log-value: " << result.value << std::endl;
  logger() << "    Value: " << exp(result.value) << std::endl;
  logger() << "    Realized by: ";
  for (auto &instantiation : result.varset) {
    logger() << instantiation.first << "->" << instantiation.second << "   ";
  }
  logger() << std::endl;
  logger() << "    Proof: " << spn->log_value(result.varset) << std::endl;
}

}  // namespace spn::actions
