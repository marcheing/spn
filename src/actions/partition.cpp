#include "actions/base.h"
#include "database/handler.h"

using std::string;

namespace spn::actions {

void partition(const ptree &params) {
  auto dataset = params.get<string>("dataset");
  auto partitioned_set_name = params.get<string>("partitioned-set-name");
  auto rest_of_set_name = params.get<string>("rest-of-set-name");
  auto proportion = params.get<double>("proportion");
  if (proportion < 0.0 || proportion > 1.0) {
    throw std::runtime_error("Parameter \"proportion\" is outside the valid range of [0.0, 1.0]");
  }

  logger() << "Executing Partition on dataset \"" << dataset
           << "\" and generating the new datasets: \"" << partitioned_set_name << " and "
           << rest_of_set_name << '\n';
  const Data &data = database::Handler::instance().data(dataset);
  const auto &[partition1, partition2] = data.random_partition(proportion);
  database::Handler::instance().insert(partitioned_set_name, partition1);
  database::Handler::instance().insert(rest_of_set_name, partition2);
}

}  // namespace spn::actions
