#include "indep/graph.h"
#include "indep/union-find.h"
#include "log.h"
#include "vardata.h"

#include <gsl/gsl_cdf.h>
#include <cmath>
#include <unordered_map>
#include <vector>

using std::unordered_map;
using std::vector;
using std::endl;

namespace spn::indep {
Graph union_find_independency_graph(const vector<VarData> &data, double pval) {
  Graph independency_graph = {unordered_map<uint64_t, vector<uint64_t>>(),
                              vector<vector<uint64_t>>(0)};

  size_t n = data.size();

  vector<uint64_t> ids(n);
  unordered_map<uint64_t, uint64_t> reverse_ids;

  for (size_t i = 0; i < n; i++) {
    ids[i] = data[i].var_id;
    reverse_ids[ids[i]] = i;
    independency_graph.adjacency_list[ids[i]] = vector<uint64_t>();
  }

  vector<UnionFindNode *> sets(n, nullptr);

  // At first, every vertex has its own set
  for (uint64_t i = 0; i < n; i++) {
    sets[i] = new UnionFindNode{ids[i]};
  }

  logger() << "Constructing independence graph..." << std::endl;

  // Construct the independence graph by adding an edge if there exists a
  // dependency relation
  for (uint64_t i = 0; i < n; i++) {
    for (uint64_t j = i + 1; j < n; j++) {
      uint64_t v1 = ids[i], v2 = ids[j];
      if (sets[i]->find() == sets[j]->find()) {
        continue;
      }

      auto p = static_cast<size_t>(data[i].n_categories),
           q = static_cast<size_t>(data[j].n_categories);

      vector<vector<int>> contingency_matrix(p + 1);
      for (size_t k = 0; k < p + 1; k++) {
        contingency_matrix[k] = vector<int>(q + 1, 0);
      }

      // data[i].data.size() == data[j].data.size() by definition
      size_t m = data[i].data.size();
      for (size_t k = 0; k < m; k++) {
        contingency_matrix[static_cast<size_t>(data[i].data[k])]
                          [static_cast<size_t>(data[j].data[k])]++;
      }

      vector<int> total_x_axis(q, 0);
      size_t total_y_axis = 0, total_x_plus_y = 0;

      for (size_t x = 0; x < p; x++) {
        total_y_axis = 0;
        for (size_t y = 0; y < q; y++) {
          total_y_axis += static_cast<size_t>(contingency_matrix[x][y]);
          total_x_axis[y] += contingency_matrix[x][y];
        }
        contingency_matrix[x][q] = static_cast<int>(total_y_axis);
      }

      for (size_t y = 0; y < q; y++) {
        contingency_matrix[p][y] = total_x_axis[y];
        total_x_plus_y += static_cast<size_t>(total_x_axis[y]);
      }

      contingency_matrix[p][q] = static_cast<int>(total_x_plus_y);

      bool indep = gtest(p, q, contingency_matrix, pval);

      if (!indep) {
        independency_graph.adjacency_list[v1].push_back(v2);
        independency_graph.adjacency_list[v2].push_back(v1);
        node_union(sets[i], sets[j]);
      }
    }
  }

  independency_graph.kset = vector<vector<uint64_t>>();
  for (uint64_t i = 0; i < n; i++) {
    if (sets[i] == sets[i]->parent()) {
      independency_graph.kset.push_back(sets[i]->var_ids());
    }
  }

  for (UnionFindNode *union_find_node : sets) {
    delete union_find_node;
  }

  return independency_graph;
}

bool gtest(size_t p, size_t q, const vector<vector<int>> &data, double sigval) {
  logger() << "Performing gtest..." << endl;
  vector<vector<double>> E(p);

  for (size_t i = 0; i < p; i++) {
    E[i] = vector<double>(q);
    for (size_t j = 0; j < q; j++) {
      E[i][j] = static_cast<double>(data[p][j] * data[i][q]) / static_cast<double>(data[p][q]);
    }
  }

  size_t df = (p - 1) * (q - 1);
  double sum = 0.0;

  for (size_t i = 0; i < p; i++) {
    for (size_t j = 0; j < q; j++) {
      if (E[i][j] == 0.0) {
        continue;
      }
      auto o = static_cast<double>(data[i][j]);
      if (o == 0.0) {
        continue;
      }
      sum += o * log(o / E[i][j]);
    }
  }

  sum *= 2;
  double cmp = 1.0 - gsl_cdf_chisq_P(sum, static_cast<double>(df));
  return cmp >= sigval;
}

}  // namespace spn::indep
