#ifndef _SPN_INDEP_GRAPH_H_
#define _SPN_INDEP_GRAPH_H_

#include "vardata.h"

#include <unordered_map>
#include <vector>

namespace spn::indep {
struct Graph {
  // Adjacency list containing each vertex and to which other vertices it is
  // connected to and from
  std::unordered_map<uint64_t, std::vector<uint64_t>> adjacency_list;

  // This k-set contains the connected subgraphs that are completely
  // separated from each other
  std::vector<std::vector<uint64_t>> kset;
};

// Creates a graph using the Union-find heuristic
Graph union_find_independency_graph(const std::vector<VarData> &data, double pval);

// G-Test - Maximum likelihood statistical significance
bool gtest(size_t p, size_t q, const std::vector<std::vector<int>> &data, double sigval);
}  // namespace spn::indep

#endif
