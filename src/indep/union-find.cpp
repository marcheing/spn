#include "indep/union-find.h"

#include <cstdint>

using std::vector;

namespace spn::indep {

UnionFindNode *UnionFindNode::find() {
  if (this != this->_parent) {
    this->_parent = this->_parent->find();
  }
  return this->_parent;
}

vector<uint64_t> UnionFindNode::var_ids() const {
  uint64_t n = this->_children.size();
  if (n == 0) {
    return vector<uint64_t>{this->_var_id};
  }
  vector<uint64_t> children{this->_var_id};
  for (uint64_t i = 0; i < n; i++) {
    vector<uint64_t> children_var_ids = this->_children[i]->var_ids();
    children.insert(children.end(), children_var_ids.begin(), children_var_ids.end());
  }
  return children;
}

UnionFindNode *node_union(UnionFindNode *x, UnionFindNode *y) {
  x = x->find();
  y = y->find();
  if (x == y) {
    return nullptr;
  }
  if (x->rank() > y->rank()) {
    y->parent(x);
    x->add_child(y);
    return x;
  }
  x->parent(y);
  y->add_child(x);
  if (x->rank() == y->rank()) {
    y->increment_rank();
  }
  return y;
}

}  // namespace spn::indep
