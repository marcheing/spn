#ifndef _SPN_INDEP_UNION_FIND_H_
#define _SPN_INDEP_UNION_FIND_H_

#include <cstdint>
#include <vector>

using std::vector;

namespace spn::indep {

class UnionFindNode {
 public:
  UnionFindNode() : _children(){};
  UnionFindNode(int rank, UnionFindNode *parent, uint64_t var_id, vector<UnionFindNode *> children)
      : _rank(rank), _parent(parent), _var_id(var_id), _children(move(children)){};

  UnionFindNode(const UnionFindNode &other)
      : UnionFindNode(other._rank, other._parent, other._var_id, other._children){};
  UnionFindNode(UnionFindNode &&union_find_node) = default;

  UnionFindNode &operator=(const UnionFindNode &other) = default;
  UnionFindNode &operator=(UnionFindNode &&other) = default;
  ~UnionFindNode() = default;

  // Named MakeSet on the original code
  explicit UnionFindNode(uint64_t var_id)
      : UnionFindNode(0, this, var_id, vector<UnionFindNode *>(0)){};
  UnionFindNode *find();

  int rank() const { return _rank; }
  UnionFindNode *parent() { return this->_parent; }
  void parent(UnionFindNode *new_parent) { this->_parent = new_parent; }
  void add_child(UnionFindNode *child) { this->_children.push_back(child); }
  void increment_rank() { this->_rank++; }
  vector<uint64_t> var_ids() const;

 private:
  int _rank{};  // Length of longest path from root to leaf
  UnionFindNode *_parent{};
  uint64_t _var_id{};
  vector<UnionFindNode *> _children;
};

UnionFindNode *node_union(UnionFindNode *x, UnionFindNode *y);

}  // namespace spn::indep

#endif
