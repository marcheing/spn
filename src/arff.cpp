#include "arff.h"
#include "data.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/split.hpp>

using boost::iequals;
using std::cerr;
using std::endl;
using std::string;
using std::unordered_map;
using std::vector;

namespace spn {

void Arff::read_file() {
  std::ifstream file(_filename, std::ios::in);
  _relation_name = read_relation_name(file);
  _attributes = read_attributes(file);
  _data = read_data(file);
  fill_attribute_variations();
}

void Arff::fill_attribute_variations() {
  for (vector<string> data_row : _data) {
    for (uint64_t i = 0; i < data_row.size(); i++) {
      if (_attributes[i].datatype().empty()) {
        continue;  // The nominal specification already has variations
      }
      vector<string> &variations = _attributes[i].variations();
      if (std::find(variations.begin(), variations.end(), data_row[i]) == variations.end()) {
        variations.push_back(data_row[i]);
      }
    }
  }
}

string Arff::read_relation_name(std::ifstream &file) const {
  string token;
  file >> token;
  while (token.front() != '@') {
    file >> token;
  }
  if (iequals(token, "@relation")) {
    file >> token;
    return token;
  }
  cerr << "Arff file " << _filename << " not formatted correctly. No @relation field found."
       << endl;
  std::exit(1);
}

vector<Attribute> Arff::read_attributes(std::ifstream &file) const {
  string token;
  vector<Attribute> attributes;
  file >> token;
  for (; !iequals(token, "@data"); file >> token) {
    while (token.front() != '@') {
      file >> token;  // Ignore blank lines and comments
    }
    if (iequals(token, "@attribute")) {
      attributes.push_back(read_attribute(file));
    }
  }
  return attributes;
}

Attribute Arff::read_attribute(std::ifstream &file) const {
  string name, datatype;
  file >> name;
  file >> datatype;
  if (iequals(datatype, "numeric") || iequals(datatype, "string") || iequals(datatype, "integer") ||
      iequals(datatype, "real")) {
    return Attribute(name, datatype);
  }
  if (iequals(datatype, "date")) {
    string date_format;
    file >> date_format;
    return Attribute(name, datatype, date_format);
  }
  // Nominal Specification
  datatype.erase(
      std::remove_if(datatype.begin(), datatype.end(), [](char x) { return x == '{' || x == '}'; }),
      datatype.end());
  vector<string> possible_values;
  boost::split(possible_values, datatype, [](char x) { return x == ','; });
  return Attribute(name, datatype, possible_values);
}

vector<vector<string>> Arff::read_data(std::ifstream &file) const {
  vector<vector<string>> data;
  vector<string> row;
  string line;
  file >> line;
  while (!file.eof()) {
    if (line.front() != '%') {
      boost::split(row, line, [](char x) { return x == ','; });
      data.push_back(row);
    }
    file >> line;
  }
  return data;
}

Data Arff::to_data() const {
  vector<unordered_map<string, uint64_t>> full_data_map;
  vector<DataVariable> variables;
  vector<unordered_map<uint64_t, uint64_t>> instances;
  for (uint64_t i = 0; i < _attributes.size(); i++) {
    unordered_map<string, uint64_t> data_map;
    for (vector<string> data_row : _data) {
      const vector<string> &variations = _attributes[i].variations();
      try {
        data_map.at(data_row[i]);
      } catch (const std::out_of_range &e) {
        data_map[data_row[i]] = static_cast<uint64_t>(
            std::find(variations.begin(), variations.end(), data_row[i]) - variations.begin());
      }
    }
    full_data_map.push_back(data_map);
    variables.emplace_back(i, data_map.size());
  }
  for (vector<string> data_row : _data) {
    unordered_map<uint64_t, uint64_t> instance_row;
    for (uint64_t i = 0; i < data_row.size(); i++) {
      instance_row[i] = full_data_map[i].at(data_row[i]);
    }
    instances.emplace_back(instance_row);
  }
  return Data(variables, instances);
}
}  // namespace spn
