#ifndef _SPN_VARDATA_H_
#define _SPN_VARDATA_H_

#include <cstdint>
#include <vector>

namespace spn {
struct VarData {
  VarData() : VarData(0, 0, {}){};
  VarData(uint64_t v, uint64_t n, std::vector<uint64_t> d) : var_id(v), n_categories(n), data(move(d)){};
  uint64_t var_id;
  uint64_t n_categories;
  std::vector<uint64_t> data;
};
}  // namespace spn

#endif
