#ifndef _SPN_INTERFACE_MAIN_APP_H_
#define _SPN_INTERFACE_MAIN_APP_H_

#include "interface/mis-db-view.h"

#include <QAction>
#include <QApplication>
#include <QCloseEvent>
#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <string>

namespace spn::interface {

class MainApp : public QMainWindow {
 public:
  MainApp();

  MainApp(const MainApp &) = delete;
  MainApp(MainApp &&main_app) = delete;
  MainApp &operator=(const MainApp &) = delete;
  MainApp &operator=(MainApp &&) = delete;
  ~MainApp() = default;

  void run();
  QStatusBar &status_bar() { return _status_bar; };

 private slots:
  void select_graph();

 private:
  void _read_settings();
  void _write_settings();
  void _init_menu_bar();
  void _init_status_bar();
  void _init_db();

  const std::string _version;
  QStatusBar _status_bar;
  MisDBView _mis_table;

 protected:
  void closeEvent(QCloseEvent *event) override;
};

}  // namespace spn::interface

#endif
