#ifndef _SPN_INTERFACE_MIS_DB_VIEW_H
#define _SPN_INTERFACE_MIS_DB_VIEW_H

#include <QSqlRelationalTableModel>
#include <QTableView>
#include <QVBoxLayout>
#include <QWidget>

namespace spn::interface {
class MisDBView : public QWidget {
 public:
  MisDBView();

  MisDBView(const MisDBView &) = default;
  MisDBView(MisDBView &&main_app) = default;
  MisDBView &operator=(const MisDBView &) = default;
  MisDBView &operator=(MisDBView &&) = default;
  ~MisDBView() = default;

  void update() { _mis_model->select(); }

 private:
  void _init_db();

  QTableView *_mis_table;
  QVBoxLayout _layout;
  QSqlRelationalTableModel *_mis_model;
};
}  // namespace spn::interface

#endif
