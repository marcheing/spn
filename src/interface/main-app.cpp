#include "interface/main-app.h"
#include "graph/mis-instance.h"
#include "utils/file.h"

#include <QByteArray>
#include <QCoreApplication>
#include <QDesktopWidget>
#include <QFileDialog>
#include <QLabel>
#include <QMenu>
#include <QRect>
#include <QSettings>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlRelationalTableModel>
#include <QSqlTableModel>
#include <QTableView>
#include <QVBoxLayout>
#include <QWidget>

#include <iostream>

namespace spn::interface {

MainApp::MainApp() : _version("0.0.0-alpha"), _status_bar(), _mis_table() {
  _read_settings();
  _init_menu_bar();
  _init_status_bar();
  setCentralWidget(&_mis_table);
}

void MainApp::_init_status_bar() {
  setStatusBar(&_status_bar);
  _status_bar.showMessage("Ready");
}

void MainApp::_init_menu_bar() {
  auto menu_bar = new QMenuBar();
  auto file_menu = new QMenu("File");
  menu_bar->addMenu(file_menu);
  auto close = new QAction("Close");
  auto new_graph = new QAction("New MIS from File...");
  file_menu->addAction(close);
  file_menu->addAction(new_graph);
  QObject::connect(close, &QAction::triggered, this, &QMainWindow::close);
  QObject::connect(new_graph, &QAction::triggered, this, &MainApp::select_graph);
  this->setMenuBar(menu_bar);
}

void MainApp::_init_db() {}

void MainApp::_read_settings() {
  QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
  const QByteArray geometry = settings.value("geometry", QByteArray()).toByteArray();
  if (geometry.isEmpty()) {
    const QRect available_geometry = QApplication::desktop()->availableGeometry(this);
    this->resize(available_geometry.width() / 3, available_geometry.height() / 2);
    this->move((available_geometry.width() - this->width()) / 2,
               (available_geometry.height() - this->height()) / 2);
  } else {
    this->restoreGeometry(geometry);
  }
}

void MainApp::_write_settings() {
  QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
  settings.setValue("geometry", this->saveGeometry());
}

void MainApp::closeEvent(QCloseEvent *event) {
  _write_settings();
  event->accept();
}

void MainApp::select_graph() {
  auto filename = QFileDialog::getOpenFileName(this, tr("Choose a .mis file"), "./",
                                               tr("Maximal independent set (MIS) files (*.mis)"));
  std::string graph_string = utils::file_to_string(filename.toStdString().c_str());
  QSqlQuery query;
  query.prepare("insert into mis (fields) values (?);");
  query.addBindValue(graph_string.c_str());
  query.exec();
  _mis_table.update();
}

}  // namespace spn::interface
