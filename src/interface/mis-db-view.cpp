#include "interface/mis-db-view.h"
#include "utils/file.h"

#include <QLabel>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>

#include <iostream>

namespace spn::interface {
MisDBView::MisDBView() : _mis_table(nullptr), _layout(), _mis_model(nullptr) {
  _init_db();
  show();
}

void MisDBView::_init_db() {
  QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
  db.setDatabaseName("spn-db.sqlite");
  db.open();
  QSqlQuery query;
  if (!query.exec(utils::file_to_string("config/database.sql").c_str())) {
    std::cout << query.lastError().text().toStdString() << std::endl;
    exit(EXIT_FAILURE);
  }
  _mis_model = new QSqlRelationalTableModel;
  _mis_table = new QTableView;
  _mis_model->setTable("mis");
  _mis_model->setEditStrategy(QSqlTableModel::OnManualSubmit);
  _mis_model->select();
  _mis_table->setModel(_mis_model);
  _mis_table->setAlternatingRowColors(true);
  _layout.addWidget(_mis_table);
  _layout.addWidget(new QLabel("Label"));
  setLayout(&_layout);
}

}  // namespace spn::interface
