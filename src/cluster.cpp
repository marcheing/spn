#include "cluster.h"
#include "indep/union-find.h"
#include "log.h"
#include "utils/metrics.h"
#include "utils/random.h"

#include <algorithm>
#include <climits>
#include <iostream>
#include <limits>
#include <queue>
#include <unordered_map>
#include <vector>

using std::unordered_map;
using std::vector;

namespace spn {

vector<unordered_map<uint64_t, const vector<uint64_t> *>> dbscan(vector<vector<uint64_t>> data,
                                                                 double eps, size_t mp) {
  uint64_t n = data.size();

  auto metric_function = utils::euclidean;

  vector<vector<double>> distance_matrix(n);
  for (uint64_t i = 0; i < n; i++) {
    distance_matrix[i] = vector<double>(n);
    for (uint64_t j = 0; j < n; j++) {
      distance_matrix[i][j] = metric_function(data[i], data[j]);
    }
  }

  vector<indep::UnionFindNode> regions(n);
  for (size_t i = 0; i < n; i++) {
    regions[i] = indep::UnionFindNode(i);
  }

  // Visited points: 0 unvisited, 1 otherwise
  vector<int> visited_points(n, 0);
  uint64_t visited_index = 0;

  std::queue<int> queue;
  queue.push(0);
  while (!queue.empty()) {
    auto p = static_cast<size_t>(queue.front());
    queue.pop();

    // Neighbourhood of p
    std::queue<int> neighbourhood;

    for (size_t i = 0; i < n; i++) {
      // Clause 1 (i != p):
      //  Pairs must be distinct
      // Clause 2 (distance_matrix[p][i] <= eps):
      //  Distance must be <= the epsilon parameter of max distance
      // Clause 3 (regions[i].find() != regions[p].find()):
      //  Pair is not already in the same cluster
      if ((i != p) && (distance_matrix[p][i] <= eps) && (regions[i].find() != regions[p].find())) {
        neighbourhood.push(static_cast<int>(i));
      }
    }

    // Found dense neighbourhood
    // FIXME: Remove the static cast
    if (neighbourhood.size() >= mp) {
      while (!neighbourhood.empty()) {
        auto q = static_cast<size_t>(neighbourhood.front());
        neighbourhood.pop();
        node_union(&regions[p], &regions[q]);
        visited_points[p] = 1;
        visited_points[q] = 1;
        queue.push(static_cast<int>(q));
      }
    }

    // Cluster has been formed. Select next non-clustered region
    if (queue.empty()) {
      for (size_t i = visited_index; i < n; i++) {
        if (visited_points[i] == 0) {
          queue.push(static_cast<int>(i));
          visited_index = i + 1;
        }
      }
    }
  }

  // Convert UnionFindNode format to vector<unordered_map<uint64_t, vector<uint64_t>>>
  // format
  uint64_t k = 0;
  vector<unordered_map<uint64_t, const vector<uint64_t> *>> clusters;
  for (uint64_t i = 0; i < n; i++) {
    if (regions[i].parent() == &regions[i]) {
      clusters.emplace_back(unordered_map<uint64_t, const vector<uint64_t> *>());
      vector<uint64_t> children = regions[i].var_ids();
      uint64_t n_children = children.size();
      for (uint64_t j = 0; j < n_children; j++) {
        size_t l = children[j];
        clusters[k][l] = &data[l];
      }
      k++;
    }
  }
  return clusters;
}

vector<OpticsObject> get_neighbors(const vector<OpticsObject> &set, const OpticsObject &o,
                                   double eps) {
  vector<OpticsObject> nset;
  uint64_t n = set.size();
  const vector<uint64_t> &p1 = o.data;
  for (uint64_t i = 0; i < n; i++) {
    if (o.index == i) {
      continue;
    }
    if (utils::euclidean(p1, set[i].data) <= eps) {
      nset.push_back(set[i]);
    }
  }
  return nset;
}

double get_core_dist(const vector<OpticsObject> &neighbors, const OpticsObject &o, size_t mp) {
  size_t n = neighbors.size();
  if (n < mp) {
    return std::numeric_limits<double>::infinity();
  }
  vector<OpticsObjectDistance> dists(n);
  const vector<uint64_t> &p1 = o.data;
  for (uint64_t i = 0; i < n; i++) {
    dists.push_back(
        OpticsObjectDistance{utils::euclidean(p1, neighbors[i].data), static_cast<int>(i)});
  }
  std::sort(dists.begin(), dists.end(),
            [](OpticsObjectDistance &a, OpticsObjectDistance &b) { return a.dist < b.dist; });
  return dists[mp - 1].dist;
}

void seeds_update(const vector<OpticsObject> &neighbors, OpticsObject *o,
                  vector<OpticsQueueItem> *pqueue) {
  double cdist = o->cdist;
  uint64_t n = neighbors.size();
  vector<uint64_t> &p1 = o->data;
  for (uint64_t i = 0; i < n; i++) {
    if (!neighbors[i].vst) {
      double rdist = std::max(cdist, utils::euclidean(p1, neighbors[i].data));
      if (o->rdist == std::numeric_limits<double>::infinity()) {
        o->rdist = rdist;
        pqueue->push_back(OpticsQueueItem{o, rdist, 0});
        pqueue->back().index = static_cast<int>(pqueue->size() - 1);
      } else if (rdist < o->rdist) {
        o->rdist = rdist;
        for (uint64_t j = 0; j < pqueue->size(); j++) {
          for (OpticsQueueItem &queue_item : *pqueue) {
            if (queue_item.object == o) {
              queue_item.object = o;
              queue_item.p = rdist;
              break;
            }
          }
        }
      }
    }
  }
}

void expand(const vector<OpticsObject> &set, OpticsObject *o, double eps, size_t mp,
            vector<OpticsObject> *order, vector<OpticsQueueItem> *pq) {
  vector<OpticsObject> neighbors = get_neighbors(set, *o, eps);
  o->vst = true;
  o->rdist = std::numeric_limits<int>::max();
  o->cdist = get_core_dist(neighbors, *o, mp);
  order->push_back(*o);
  if (o->cdist != std::numeric_limits<int>::max()) {
    seeds_update(neighbors, o, pq);
    while (!pq->empty()) {
      OpticsObject *co = pq->front().object;
      pq->erase(pq->begin());
      neighbors = get_neighbors(set, *co, eps);
      co->vst = true;
      co->cdist = get_core_dist(neighbors, *co, mp);
      order->push_back(*co);
      if (co->cdist != std::numeric_limits<double>::infinity()) {
        seeds_update(neighbors, co, pq);
      }
    }
  }
}

vector<unordered_map<uint64_t, const vector<uint64_t> *>> extract(const vector<OpticsObject> &order,
                                                                  double eps) {
  size_t idtrk = 0;
  vector<vector<OpticsObject>> cids(1);

  uint64_t n = order.size();
  const int noise = 0;
  for (uint64_t i = 0; i < n; i++) {
    const OpticsObject &o = order.at(i);
    if (o.rdist > eps) {
      if (o.cdist <= eps) {
        idtrk++;
        vector<OpticsObject> stub(1);
        stub[0] = o;
        cids.push_back(stub);
      } else {
        cids[noise].push_back(o);
      }
    } else {
      cids[idtrk].push_back(o);
    }
  }

  uint64_t m = cids.size();
  vector<unordered_map<uint64_t, const vector<uint64_t> *>> clusters(m);
  for (uint64_t i = 0; i < m; i++) {
    clusters[i] = unordered_map<uint64_t, const vector<uint64_t> *>();
    uint64_t p = cids[i].size();
    for (uint64_t j = 0; j < p; j++) {
      OpticsObject &o = cids[i][j];
      clusters[i][o.index] = &o.data;
    }
  }

  return clusters;
}

vector<unordered_map<uint64_t, const vector<uint64_t> *>> optics(vector<vector<uint64_t>> data,
                                                                 double eps, size_t mp) {
  uint64_t n = data.size();
  vector<OpticsObject> order;
  vector<OpticsObject> set(n);
  vector<OpticsQueueItem> pq;

  for (uint64_t i = 0; i < n; i++) {
    set[i] = OpticsObject{data[i], false, i, std::numeric_limits<double>::infinity(),
                          std::numeric_limits<double>::infinity()};
  }

  for (uint64_t i = 0; i < n; i++) {
    OpticsObject obj = set[i];
    if (!obj.vst) {
      expand(set, &obj, eps, mp, &order, &pq);
    }
  }

  return extract(order, eps);
}

void kmedoidinsert(size_t which, vector<uint64_t> *means,
                   vector<unordered_map<uint64_t, const vector<uint64_t> *>> *clusters,
                   const vector<uint64_t> &v, uint64_t i) {
  (*clusters)[which][i] = &v;
  size_t smean = 0, s = 0;
  for (std::pair<uint64_t, const vector<uint64_t> *> value : clusters->at(which)) {
    smean += utils::hamming(*clusters->at(which)[means->at(which)], *value.second);
    s += utils::hamming(v, *value.second);
  }
  if (s < smean) {
    means->at(which) = i;
  }
}

void kmedoidremove(size_t which, vector<uint64_t> *means,
                   vector<unordered_map<uint64_t, const vector<uint64_t> *>> *clusters,
                   uint64_t i) {
  clusters->at(which).erase(i);
  if (means->at(which) == i) {
    size_t best = std::numeric_limits<size_t>::max();
    for (std::pair<uint64_t, const vector<uint64_t> *> value : clusters->at(which)) {
      uint64_t j = value.first;
      size_t s = 0;
      for (std::pair<uint64_t, const vector<uint64_t> *> v : clusters->at(which)) {
        s += utils::hamming(*v.second, *value.second);
      }
      if (s < best) {
        best = s;
        means->at(which) = j;
      }
    }
  }
}

vector<unordered_map<uint64_t, const vector<uint64_t> *>> kmedoid(
    size_t k, const vector<vector<uint64_t>> &data) {
  size_t n = data.size();

  // Initializes using the Forgy method
  unordered_map<uint64_t, bool> chkrnd;
  vector<unordered_map<uint64_t, const vector<uint64_t> *>> clusters(1);
  vector<uint64_t> means(1);
  unordered_map<uint64_t, uint64_t> chkdata;

  for (size_t i = 0; i < k; i++) {
    uint64_t r;
    bool ok = true;
    while (ok && chkrnd.size() < n) {
      for (ok = true; ok; ok = (chkrnd.count(r) > 0)) {
        r = utils::generate_random<uint64_t>(0, n - 1);
      }
      chkrnd[r] = true;
      for (size_t ii = 0; ii < i && !ok; ii++) {
        size_t lr = data[r].size();
        size_t j = 0;
        while (j < lr) {
          if (data[r][j] != data[means[ii]][j]) {
            break;
          }
          j++;
        }
        if (j >= lr) {
          ok = true;
        }
      }
    }
    if (ok) {
      break;
    }
    if (i > 0) {
      clusters.emplace_back(unordered_map<uint64_t, const vector<uint64_t> *>());
      means.emplace_back(r);
    } else {
      clusters[0] = unordered_map<uint64_t, const vector<uint64_t> *>();
      means[0] = r;
    }
    chkdata[r] = i;
    kmedoidinsert(i, &means, &clusters, data[r], r);
  }

  logger() << "Starting K-means until convergence..." << std::endl;

  uint64_t nochange = 0;
  size_t i = 0;
  while (nochange < n) {
    size_t min = data[i].size() + 1;
    size_t which = std::numeric_limits<size_t>::max();
    if (chkdata.count(i) > 0) {
      which = chkdata[i];
      min = utils::hamming(*clusters[which][means[which]], data[i]);
    }
    for (size_t j = 0; j < k; j++) {
      if (j != which) {
        size_t t = utils::hamming(*clusters[j][means[j]], data[i]);
        if (t < min) {
          min = t;
          which = j;
        }
      }
    }
    if (chkdata.count(i) > 0) {
      uint64_t v = chkdata[i];
      if (v != which) {
        // If instance has an earlier attached cluster
        kmedoidremove(chkdata[i], &means, &clusters, i);
        chkdata[i] = which;
        kmedoidinsert(which, &means, &clusters, data[i], i);
        nochange = 0;
      } else {
        nochange++;
      }
    } else {  // Instance i has no attached cluster
      chkdata[i] = which;
      kmedoidinsert(which, &means, &clusters, data[i], i);
      nochange = 0;
    }
    i++;
    if (i >= n) {
      i = 0;
    }
  }

  clusters = vector<unordered_map<uint64_t, const vector<uint64_t> *>>(k);
  for (size_t ii = 0; ii < k; ii++) {
    clusters[ii] = unordered_map<uint64_t, const vector<uint64_t> *>();
  }
  i = 0;
  while (i < n) {
    clusters[chkdata[i]][i] = &data[i];
    i++;
  }

  return clusters;
}
}  // namespace spn
