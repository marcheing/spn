#ifndef _SPN_DATABASE_HANDLER_H_
#define _SPN_DATABASE_HANDLER_H_

#include "data.h"
#include "graph/mis-instance.h"
#include "node/spn.h"

#include <memory>
#include <string>
#include <unordered_map>

namespace spn::database {

class Handler {
 public:
  Handler(const Handler &handler) = delete;
  Handler(Handler &&handler) = delete;
  Handler &operator=(const Handler &) = delete;
  Handler &operator=(Handler &&) = delete;
  ~Handler() {
    for (const auto &[_, spn] : _spn_map) delete spn;
  };

  static Handler &instance() {
    static Handler handler;
    return handler;
  }

  const node::SPN *spn(const std::string &name) const;
  const Data &data(const std::string &name) const;
  const graph::MISInstance &mis_instance(const std::string &name) const;
  void insert(const std::string &name, const node::SPN *spn);
  void insert(const std::string &name, const Data &data);
  void insert(const std::string &name, const graph::MISInstance &mis_instance);

 private:
  Handler() : _spn_map(), _data_map(), _mis_instance_map(){};
  std::unordered_map<std::string, const node::SPN *> _spn_map;
  std::unordered_map<std::string, Data> _data_map;
  std::unordered_map<std::string, graph::MISInstance> _mis_instance_map;
};  // namespace spn::database

}  // namespace spn::database

#endif
