#include "database/handler.h"

#include <memory>

using std::string;

namespace spn::database {

const node::SPN *Handler::spn(const string &name) const { return _spn_map.at(name); }

const Data &Handler::data(const string &name) const { return _data_map.at(name); }

const graph::MISInstance &Handler::mis_instance(const string &name) const {
  return _mis_instance_map.at(name);
}

void Handler::insert(const string &name, const node::SPN *spn) { _spn_map.insert({name, spn}); }

void Handler::insert(const string &name, const Data &data) { _data_map.insert({name, data}); }

void Handler::insert(const string &name, const graph::MISInstance &mis_instance) {
  _mis_instance_map.insert({name, mis_instance});
}

}  // namespace spn::database
